package ro.dragos.smartfarm.smartfarm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;

@SpringBootApplication
@ComponentScan(basePackages={"ro.dragos.smartfarm.controllers", "ro.dragos.smartfarm.smartfarm", "ro.dragos.smartfarm.config",
		"ro.dragos.smartfarm.service"})
@EntityScan(basePackages={"ro.dragos.smartfarm.model"})
@EnableJpaRepositories("ro.dragos.smartfarm.dao")
//@EnableAuthorizationServer


public class SmartfarmApplication {

	
//	@Value("classpath:schema.sql")
//	private Resource schemaScript;
	
	
//	@Autowired
//	private DataSource dataSource;
	
	public static void main(String[] args) {
		SpringApplication.run(SmartfarmApplication.class, args);
	}
	
	
	/* ----- oauth scripts and resources ------ */
	
	
//	@Bean
//	public DataSourceInitializer dataSourceInitializer(DataSource dataSource) {
//	    DataSourceInitializer initializer = new DataSourceInitializer();
//	    initializer.setDataSource(dataSource);
//	    initializer.setDatabasePopulator(databasePopulator());
//	    return initializer;
//	}
//	 
//	
//	private DatabasePopulator databasePopulator() {
//	    ResourceDatabasePopulator populator = new ResourceDatabasePopulator();
//	    populator.addScript(schemaScript);
//	    return populator;
//	}
	 

}

