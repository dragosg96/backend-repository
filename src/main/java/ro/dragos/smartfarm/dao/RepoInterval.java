package ro.dragos.smartfarm.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ro.dragos.smartfarm.model.OpInterval;
import ro.dragos.smartfarm.model.OpIntervalPK;

@Repository
public interface RepoInterval extends CrudRepository<OpInterval, OpIntervalPK>{

	
	public List<OpInterval> findByPkIdSector(Integer idSector);
}
