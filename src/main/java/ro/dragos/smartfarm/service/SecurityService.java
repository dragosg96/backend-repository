package ro.dragos.smartfarm.service;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ro.dragos.smartfarm.dao.RepoUser;
import ro.dragos.smartfarm.model.User;

@Service
public class SecurityService {

	
	@Autowired 
	private RepoUser repoUser;
	
	
	public User findLoggedUserFromPrincipal(Principal principal){
		User loggedInUser = repoUser.findByUsername(principal.getName()).get();
		return loggedInUser;
	}
	
	
}
