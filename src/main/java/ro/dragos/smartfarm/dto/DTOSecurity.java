package ro.dragos.smartfarm.dto;

import ro.dragos.smartfarm.config.IConfigConstants;

public class DTOSecurity {

	private String username;
	private String userType;
	
	public DTOSecurity() {
		// TODO Auto-generated constructor stub
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}
	
	
	
	
}
