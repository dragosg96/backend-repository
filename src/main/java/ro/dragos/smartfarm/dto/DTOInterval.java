package ro.dragos.smartfarm.dto;

import ro.dragos.smartfarm.model.FarmOperation;
import ro.dragos.smartfarm.model.OpInterval;

public class DTOInterval {

	private OpInterval interval;
	private FarmOperation operation;
	
	public DTOInterval() {
	}

	public OpInterval getInterval() {
		return interval;
	}

	public void setInterval(OpInterval interval) {
		this.interval = interval;
	}

	public FarmOperation getOperation() {
		return operation;
	}

	public void setOperation(FarmOperation operation) {
		this.operation = operation;
	}
	
	
	
	
}
