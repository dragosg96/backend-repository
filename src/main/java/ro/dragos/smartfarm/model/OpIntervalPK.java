package ro.dragos.smartfarm.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class OpIntervalPK implements Serializable{


	
	@Column(name="id_farm_sector")
	private Integer idSector;
	
	@Column(name="id_farm_operation")
	private Integer idOperation;
	
	public OpIntervalPK() {
	}

	public Integer getIdSector() {
		return idSector;
	}

	public void setIdSector(Integer idSector) {
		this.idSector = idSector;
	}

	public Integer getIdOperation() {
		return idOperation;
	}

	public void setIdOperation(Integer idOperation) {
		this.idOperation = idOperation;
	}

	@Override
	public String toString() {
		return "OpIntervalPK [idSector=" + idSector + ", idOperation=" + idOperation + "]";
	}
	
	
	
}
