package ro.dragos.smartfarm.dto;

import ro.dragos.smartfarm.model.FarmOperationProperty;
import ro.dragos.smartfarm.model.SoilType;

public class DTOIrigation {

	private SoilType soilType;
	private DTOPolygon dtoPolygon; // FarmSector
	private DTOWeather dtoWeather; // FarmSector
	private FarmOperationProperty farmOperationProperty;
	private Integer farmSectorId;
	
	public DTOIrigation() {
		// TODO Auto-generated constructor stub
	}

	public DTOIrigation(SoilType soilType, DTOPolygon dtoPolygon, DTOWeather dtoWeather) {
		super();
		this.soilType = soilType;
		this.dtoPolygon = dtoPolygon;
		this.dtoWeather = dtoWeather;
	}

	public SoilType getSoilType() {
		return soilType;
	}

	public void setSoilType(SoilType soilType) {
		this.soilType = soilType;
	}

	public DTOPolygon getDtoPolygon() {
		return dtoPolygon;
	}

	public void setDtoPolygon(DTOPolygon dtoPolygon) {
		this.dtoPolygon = dtoPolygon;
	}

	public DTOWeather getDtoWeather() {
		return dtoWeather;
	}

	public void setDtoWeather(DTOWeather dtoWeather) {
		this.dtoWeather = dtoWeather;
	}
	
	

	public FarmOperationProperty getFarmOperationProperty() {
		return farmOperationProperty;
	}

	public void setFarmOperationProperty(FarmOperationProperty farmOperationProperty) {
		this.farmOperationProperty = farmOperationProperty;
	}

	
	
	public Integer getFarmSectorId() {
		return farmSectorId;
	}

	public void setFarmSectorId(Integer farmSectorId) {
		this.farmSectorId = farmSectorId;
	}

	@Override
	public String toString() {
		return "DTOIrigation [soilType=" + soilType + ", dtoPolygon=" + dtoPolygon + ", dtoWeather=" + dtoWeather + "]";
	}

	
	
}
