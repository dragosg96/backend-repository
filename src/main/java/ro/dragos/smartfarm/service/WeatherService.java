package ro.dragos.smartfarm.service;



import org.json.JSONException;
import org.json.JSONObject;

import ro.dragos.smartfarm.dto.DTOWeather;

public class WeatherService {

	private static final String METRIC_IMPERIAL = "&units=metric";
	private static final String API_KEY_WEATHER_SERVICE = "56a2ac50d2fbb400335612e7d8e41a5d" + METRIC_IMPERIAL;
	private static final String WEATHER_ENDPOINT = "http://api.openweathermap.org/data/2.5/weather";
	
	public static DTOWeather loadFromEndpoint(double lat, double lng){
		String weatherUrl = WEATHER_ENDPOINT + "?lat="+lat+"&lon="+lng+"&appid="+API_KEY_WEATHER_SERVICE+"&units=metric";
		// api.openweathermap.org/data/2.5/weather?q=Bucharest&appid=56a2ac50d2fbb400335612e7d8e41a5d
		// api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}

		System.out.println("***LOADING WEATHER DATA FOR: " + lat + " x " + lng + " FROM: " + weatherUrl);
		long start = System.currentTimeMillis();
		
		String currentWeatherJsonString = HttpHelper.load(weatherUrl);
		DTOWeather dto = WeatherService.string2Dto(currentWeatherJsonString);
		long end = System.currentTimeMillis();
		System.out.println("***END LOADING WEATHER DATA FOR: " + lat + " x " + lng );
		System.out.println("OPERATION TOOK: "  + (end-start) + " milliseconds");
		
		
		
		return dto;
	}
	
	public static DTOWeather string2Dto(String jsonString){
		
		
//		private Double windSpeed; // wind.speed
		
		DTOWeather dto = new DTOWeather();
		try {
			JSONObject jsonObject = new JSONObject(jsonString);
			dto.setName(jsonObject.getString("name"));
			dto.setHumidity(jsonObject.getJSONObject("main").getDouble("humidity"));
			dto.setTemp(jsonObject.getJSONObject("main").getDouble("temp"));
			dto.setPressure(jsonObject.getJSONObject("main").getDouble("pressure"));
			
			dto.setWindSpeed(jsonObject.getJSONObject("wind").getDouble("speed"));
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dto;
	}
	
}
