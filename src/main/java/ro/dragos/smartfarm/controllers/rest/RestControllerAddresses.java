package ro.dragos.smartfarm.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ro.dragos.smartfarm.dao.RepoAddress;
import ro.dragos.smartfarm.dao.RepoFarm;
import ro.dragos.smartfarm.model.Address;
import ro.dragos.smartfarm.model.Farm;

@RestController
@RequestMapping("/rest/addresses")
@CrossOrigin("http://localhost:4200")

public class RestControllerAddresses {

	@Autowired
	private RepoAddress repoAddress;
	
	@Autowired
	private RepoFarm repoFarm;
	
	@PutMapping("/update")
	public Address updateAddress(@RequestBody Address address){
		System.out.println("**UPDATING ADDRESS**");
		repoAddress.save(address);
		return address;
	}
	
	@PostMapping("/save/{farmId}")
	public Address updateAddress(@RequestBody Address address, @PathVariable("farmId") int farmId){
		System.out.println("**UPDATING ADDRESS**");
		Farm farm = repoFarm.findById(farmId).get();
		farm.setAddress(address);
		// repoAddress.save(address);
		repoFarm.save(farm);
		return address;
	}
	
}
