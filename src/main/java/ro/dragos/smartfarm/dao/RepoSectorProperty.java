package ro.dragos.smartfarm.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ro.dragos.smartfarm.model.FarmSector;
import ro.dragos.smartfarm.model.FarmSectorProperty;

@Repository
public interface RepoSectorProperty extends CrudRepository<FarmSectorProperty, Integer>{

	public List<FarmSectorProperty> findByFarmSector(FarmSector sectorId);
	public List<FarmSectorProperty> findByDateMeasuredBetween(Date start, Date end);
}
