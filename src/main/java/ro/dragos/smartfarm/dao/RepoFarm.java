package ro.dragos.smartfarm.dao;

import org.springframework.data.repository.CrudRepository;

import ro.dragos.smartfarm.model.Farm;

public interface RepoFarm extends CrudRepository<Farm, Integer>{

}
