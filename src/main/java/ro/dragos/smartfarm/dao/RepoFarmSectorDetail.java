package ro.dragos.smartfarm.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ro.dragos.smartfarm.model.FarmOperationProperty;
import ro.dragos.smartfarm.model.FarmSector;
import ro.dragos.smartfarm.model.FarmSectorDetail;

@Repository
public interface RepoFarmSectorDetail extends CrudRepository<FarmSectorDetail, Integer>{

	public List<FarmSectorDetail> findByFarmSector(FarmSector farmSector);
	public List<FarmSectorDetail> findByFarmOperationProperty(FarmOperationProperty farmOperationProperty);
}
