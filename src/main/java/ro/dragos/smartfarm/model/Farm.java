package ro.dragos.smartfarm.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the farms database table.
 * 
 */
@Entity
@Table(name="farms")
@NamedQuery(name="Farm.findAll", query="SELECT f FROM Farm f")
public class Farm implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	@Column(name="farm_name")
	private String farmName;

//	@Column(name="id_address")
//	private Integer idAddress;
	
	@ManyToOne(fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	@JoinColumn(name="id_address")
	private Address address;

	//bi-directional many-to-one association to FarmSector
//	@OneToMany(mappedBy="farm")
//	private List<FarmSector> farmSectors;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="id_owner")
	private User user;

	public Farm() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFarmName() {
		return this.farmName;
	}

	public void setFarmName(String farmName) {
		this.farmName = farmName;
	}

//	public Integer getIdAddress() {
//		return this.idAddress;
//	}
//
//	public void setIdAddress(Integer idAddress) {
//		this.idAddress = idAddress;
//	}

//	public List<FarmSector> getFarmSectors() {
//		return this.farmSectors;
//	}
//
//	public void setFarmSectors(List<FarmSector> farmSectors) {
//		this.farmSectors = farmSectors;
//	}

//	public FarmSector addFarmSector(FarmSector farmSector) {
//		getFarmSectors().add(farmSector);
//		farmSector.setFarm(this);
//
//		return farmSector;
//	}
//
//	public FarmSector removeFarmSector(FarmSector farmSector) {
//		getFarmSectors().remove(farmSector);
//		farmSector.setFarm(null);
//
//		return farmSector;
//	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}
	
	

}