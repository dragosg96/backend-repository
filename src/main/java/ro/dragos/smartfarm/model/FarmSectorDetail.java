package ro.dragos.smartfarm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the farm_sector_detail database table.
 * 
 */
@Entity
@Table(name="farm_sector_detail")
@NamedQuery(name="FarmSectorDetail.findAll", query="SELECT f FROM FarmSectorDetail f")
public class FarmSectorDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="application_date")
	private Date applicationDate;

	//bi-directional many-to-one association to FarmOperationProperty
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="farm_operation_property_id")
	private FarmOperationProperty farmOperationProperty;

	//bi-directional many-to-one association to FarmSector
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="id_farm_sector")
	private FarmSector farmSector;

	public FarmSectorDetail() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getApplicationDate() {
		return this.applicationDate;
	}

	public void setApplicationDate(Date applicationDate) {
		this.applicationDate = applicationDate;
	}

	public FarmOperationProperty getFarmOperationProperty() {
		return this.farmOperationProperty;
	}

	public void setFarmOperationProperty(FarmOperationProperty farmOperationProperty) {
		this.farmOperationProperty = farmOperationProperty;
	}

	public FarmSector getFarmSector() {
		return this.farmSector;
	}

	public void setFarmSector(FarmSector farmSector) {
		this.farmSector = farmSector;
	}

}