package ro.dragos.smartfarm.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="addresses")
public class Address {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY) // AUTO
	@Column(name="id")
	private Integer id;
	
	@Column(name="city")
	private String city;
	@Column(name="street")
	private String street;
	@Column(name="snumber")
	private String sNumber;
	
//	@OneToMany
//	private List<Farm> farmsAtThisAddress;
	
//	@OneToOne
//	private Farm farmAtThisAddress;
	
	public Address(){
		
	}

	public Address(String city, String street, String sNumber) {
		this.city = city;
		this.street = street;
		this.sNumber = sNumber;
	}

	public Address(Integer id, String city, String street, String sNumber) {
		this.id = id;
		this.city = city;
		this.street = street;
		this.sNumber = sNumber;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getsNumber() {
		return sNumber;
	}

	public void setsNumber(String sNumber) {
		this.sNumber = sNumber;
	}

	@Override
	public String toString() {
		return "Address [id=" + id + ", city=" + city + ", street=" + street + ", sNumber=" + sNumber + "]";
	}
	

	
	
	
	
}
