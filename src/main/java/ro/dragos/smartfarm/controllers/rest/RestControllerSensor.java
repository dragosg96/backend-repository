package ro.dragos.smartfarm.controllers.rest;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ro.dragos.smartfarm.dao.RepoFarm;
import ro.dragos.smartfarm.dao.RepoFarmSector;
import ro.dragos.smartfarm.dto.DTOFarmSectorProperty;
import ro.dragos.smartfarm.model.FarmSector;
import ro.dragos.smartfarm.model.FarmSectorProperty;

@RestController
@RequestMapping("/input/sensor")
public class RestControllerSensor {

	
	
	@Autowired
	private RepoFarm repoFarm;
	@Autowired
	private RepoFarmSector repoFarmSector;

	
	@PostMapping("/properties/add")
	public FarmSectorProperty savePropertyToFarmSector(@RequestBody DTOFarmSectorProperty dto) {
		System.out.println("===================== SAVING DTO: " + dto);
		// saveSectorProperty(sectorId, sectorPropertyName, sectorPropertyValue)
		FarmSector farmSector = repoFarmSector.findById(dto.getFarmSectorId()).get();
		FarmSectorProperty farmSectorProperty = new FarmSectorProperty();
		farmSectorProperty.setPropName(dto.getPropertyName());
		farmSectorProperty.setPropValue(dto.getPropertyValue());
		if (farmSector.getFarmSectorProperties() == null) {
			farmSector.setFarmSectorProperties(new ArrayList<>());
		}

		if(farmSectorProperty.getDateMeasured() == null){
			farmSectorProperty.setDateMeasured(new Date());
		}
		farmSectorProperty.setFarmSector(farmSector);
		farmSector.getFarmSectorProperties().add(farmSectorProperty);

		repoFarmSector.save(farmSector);

		// FarmSector farmSectorUpdated = repoFarmSector.findById(dto.getFarmSectorId()).get();

		return farmSectorProperty;
	}
	
}
