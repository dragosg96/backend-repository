package ro.dragos.smartfarm.controllers.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ro.dragos.smartfarm.dao.RepoFarmSector;
import ro.dragos.smartfarm.dto.DTOCoordinate;
import ro.dragos.smartfarm.model.FarmSector;

@RestController
@RequestMapping("/rest/geo")
@CrossOrigin("http://localhost:4200")
public class RestControllerGeocoding {

	@Autowired
	private RepoFarmSector repoFarmSector;
	
	@GetMapping("/test")
	public String test(){
		System.out.println("TEST");
		return "TEST HERE";
	}
	
	@PutMapping("/sector/update/{id}")
	public FarmSector updateFarmSectorCoordinates(@PathVariable("id") int sectorId, @RequestBody List<DTOCoordinate> coordinates){
		FarmSector sector = repoFarmSector.findById(sectorId).get();
		System.out.println("**UPDATING SECTOR COORDS: " + coordinates);
		StringBuilder dbCoords = new StringBuilder();
		
		// -28.308407131969194,143.16234763978105,-31.844117710939337,142.98656638978105,-30.41472581319501,148.74340232728105
		for(DTOCoordinate coord : coordinates){
			dbCoords.append(coord.getLat());
			dbCoords.append(",");
			dbCoords.append(coord.getLng());
			dbCoords.append(",");
		}
		String dbColumn = dbCoords.toString();
		System.out.println("BEFORE: " + dbColumn);
		dbColumn = dbColumn.substring(0, dbColumn.length() - 2);
		System.out.println("AFTER: " + dbColumn);
		sector.setCoordinates(dbColumn);
		
		repoFarmSector.save(sector);
		return sector;
	}
	
}
