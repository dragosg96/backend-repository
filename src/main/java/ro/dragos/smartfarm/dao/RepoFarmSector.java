package ro.dragos.smartfarm.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ro.dragos.smartfarm.model.Farm;
import ro.dragos.smartfarm.model.FarmSector;

@Repository
public interface RepoFarmSector extends CrudRepository<FarmSector, Integer>{

	
	public List<FarmSector> findByFarm(Farm farm);
}
