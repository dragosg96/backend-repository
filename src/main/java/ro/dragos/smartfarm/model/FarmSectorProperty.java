package ro.dragos.smartfarm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the farm_sector_property database table.
 * 
 */
@Entity
@Table(name="farm_sector_property")
@NamedQuery(name="FarmSectorProperty.findAll", query="SELECT f FROM FarmSectorProperty f")
public class FarmSectorProperty implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="prop_name")
	private String propName;

	@Column(name="prop_value")
	private String propValue;
	
	
//	@Temporal(TemporalType.)
	@Column(name="date_measured")
	private Date dateMeasured;

	//bi-directional many-to-one association to FarmSector
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="farm_sector_id")
	private FarmSector farmSector;

	public FarmSectorProperty() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPropName() {
		return this.propName;
	}

	public void setPropName(String propName) {
		this.propName = propName;
	}

	public String getPropValue() {
		return this.propValue;
	}

	public void setPropValue(String propValue) {
		this.propValue = propValue;
	}

	public FarmSector getFarmSector() {
		return this.farmSector;
	}

	public void setFarmSector(FarmSector farmSector) {
		this.farmSector = farmSector;
	}

	public Date getDateMeasured() {
		return dateMeasured;
	}

	public void setDateMeasured(Date dateMeasured) {
		this.dateMeasured = dateMeasured;
	}

	
	
}