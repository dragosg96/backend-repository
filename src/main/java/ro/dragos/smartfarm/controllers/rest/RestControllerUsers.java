package ro.dragos.smartfarm.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ro.dragos.smartfarm.dao.RepoUser;
import ro.dragos.smartfarm.model.User;

@RestController
@RequestMapping("/rest/users")
public class RestControllerUsers {

	@Autowired
	private RepoUser repoUser;
	
	@RequestMapping("/all")
	public Iterable<User> allUsers(){
		return repoUser.findAll();
	}
}
