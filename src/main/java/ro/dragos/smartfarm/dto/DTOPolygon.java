package ro.dragos.smartfarm.dto;

import java.util.ArrayList;
import java.util.List;

public class DTOPolygon {

	private List<DTOCoordinate> coordinates = new ArrayList<>();
	
	
	public DTOPolygon() {
	}


	public List<DTOCoordinate> getCoordinates() {
		return coordinates;
	}


	public void setCoordinates(List<DTOCoordinate> coordinates) {
		this.coordinates = coordinates;
	}


	@Override
	public String toString() {
		return "DTOPolygon [coordinates=" + coordinates + "]";
	}

	
	
}
