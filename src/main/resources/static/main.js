(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ku": "./node_modules/moment/locale/ku.js",
	"./ku.js": "./node_modules/moment/locale/ku.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	var module = __webpack_require__(id);
	return module;
}
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) { // check for number or string
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return id;
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/about/about.component.css":
/*!*******************************************!*\
  !*** ./src/app/about/about.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/about/about.component.html":
/*!********************************************!*\
  !*** ./src/app/about/about.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\n  About SmartFarm\n  <hr>\n  SmartFarm is a web-based solution designed to help farmers better keep track of \n  various properties pertaining to farm management. For instance,\n  farmers can track various segments of land (farm sectors), map them \n  on a visual map and also record various information about properties (such as humidity or temperature),\n  but also to address said properties by keeping track of the operations which\n  must be performed at certain intervals.\n</div>"

/***/ }),

/***/ "./src/app/about/about.component.ts":
/*!******************************************!*\
  !*** ./src/app/about/about.component.ts ***!
  \******************************************/
/*! exports provided: AboutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AboutComponent", function() { return AboutComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AboutComponent = /** @class */ (function () {
    function AboutComponent() {
    }
    AboutComponent.prototype.ngOnInit = function () {
    };
    AboutComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-about',
            template: __webpack_require__(/*! ./about.component.html */ "./src/app/about/about.component.html"),
            styles: [__webpack_require__(/*! ./about.component.css */ "./src/app/about/about.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], AboutComponent);
    return AboutComponent;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div style=\"text-align:center\">\n  \n  <img src=\"assets/img/logo.png\"  alt=\"smart-farm-logo\"\n  style=\"float:left;width: 100%;\"> \n  <h1 style=\"display: inline; margin-top: 10px;\">\n    Welcome to {{ title }}!\n  </h1>\n</div>\n<div style=\"margin-top: 50px; padding-left: 10px;\">\n  <h4>\n    Dedicated farm-management application. You can register your farms and view real-time imformation \n    and reports regarding each particular sector.\n  </h4>\n</div>\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = "SmartFarm Application";
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _farms_farms_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./farms/farms.module */ "./src/app/farms/farms.module.ts");
/* harmony import */ var _materialm_materialm_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./materialm/materialm.module */ "./src/app/materialm/materialm.module.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _farms_sector_map_sector_map_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./farms/sector-map/sector-map.component */ "./src/app/farms/sector-map/sector-map.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _farms_farms_list_farms_list_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./farms/farms-list/farms-list.component */ "./src/app/farms/farms-list/farms-list.component.ts");
/* harmony import */ var _farms_farms_details_farms_details_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./farms/farms-details/farms-details.component */ "./src/app/farms/farms-details/farms-details.component.ts");
/* harmony import */ var _farms_sector_sector_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./farms/sector/sector.component */ "./src/app/farms/sector/sector.component.ts");
/* harmony import */ var _farms_reports_sd_properties_reports_sd_properties_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./farms/reports-sd-properties/reports-sd-properties.component */ "./src/app/farms/reports-sd-properties/reports-sd-properties.component.ts");
/* harmony import */ var angular_google_charts__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! angular-google-charts */ "./node_modules/angular-google-charts/fesm5/angular-google-charts.js");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _user_user_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./user/user.component */ "./src/app/user/user.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _farms_irigation_irigation_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./farms/irigation/irigation.component */ "./src/app/farms/irigation/irigation.component.ts");
/* harmony import */ var _logoutdialog_logoutdialog_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./logoutdialog/logoutdialog.component */ "./src/app/logoutdialog/logoutdialog.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _search_search_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./search/search.component */ "./src/app/search/search.component.ts");
/* harmony import */ var _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @angular/material/autocomplete */ "./node_modules/@angular/material/esm5/autocomplete.es5.js");
/* harmony import */ var _about_about_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./about/about.component */ "./src/app/about/about.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
























var appRoutes = [
    {
        path: 'sector-map/:idsector',
        component: _farms_sector_map_sector_map_component__WEBPACK_IMPORTED_MODULE_6__["SectorMapComponent"],
    },
    {
        path: '',
        component: _app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"]
    },
    {
        path: 'app-farms',
        component: _farms_farms_list_farms_list_component__WEBPACK_IMPORTED_MODULE_8__["FarmsListComponent"]
    },
    {
        path: 'farms/:idfarm',
        component: _farms_farms_details_farms_details_component__WEBPACK_IMPORTED_MODULE_9__["FarmsDetailsComponent"]
    }, {
        path: 'farms/sectors/:idsector',
        component: _farms_sector_sector_component__WEBPACK_IMPORTED_MODULE_10__["SectorComponent"]
    },
    {
        path: 'reports/spropertis/:idsector',
        component: _farms_reports_sd_properties_reports_sd_properties_component__WEBPACK_IMPORTED_MODULE_11__["ReportsSdPropertiesComponent"]
    },
    { path: 'login', component: _login_login_component__WEBPACK_IMPORTED_MODULE_13__["LoginComponent"] },
    { path: 'irigation/:id', component: _farms_irigation_irigation_component__WEBPACK_IMPORTED_MODULE_16__["IrigationComponent"] },
    { path: 'about', component: _about_about_component__WEBPACK_IMPORTED_MODULE_22__["AboutComponent"] }
];
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"],
                _home_home_component__WEBPACK_IMPORTED_MODULE_7__["HomeComponent"],
                _login_login_component__WEBPACK_IMPORTED_MODULE_13__["LoginComponent"],
                _user_user_component__WEBPACK_IMPORTED_MODULE_14__["UserComponent"],
                _logoutdialog_logoutdialog_component__WEBPACK_IMPORTED_MODULE_17__["LogoutdialogComponent"],
                _search_search_component__WEBPACK_IMPORTED_MODULE_20__["SearchComponent"],
                _about_about_component__WEBPACK_IMPORTED_MODULE_22__["AboutComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _farms_farms_module__WEBPACK_IMPORTED_MODULE_3__["FarmsModule"],
                _materialm_materialm_module__WEBPACK_IMPORTED_MODULE_4__["MaterialmModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterModule"].forRoot(appRoutes, { enableTracing: true } // <-- debugging purposes only
                ),
                angular_google_charts__WEBPACK_IMPORTED_MODULE_12__["GoogleChartsModule"].forRoot(),
                _angular_forms__WEBPACK_IMPORTED_MODULE_15__["FormsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_18__["MatDialogModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_19__["HttpClientModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_15__["ReactiveFormsModule"],
                _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_21__["MatAutocompleteModule"]
            ],
            providers: [],
            bootstrap: [_home_home_component__WEBPACK_IMPORTED_MODULE_7__["HomeComponent"]],
            entryComponents: [
                _logoutdialog_logoutdialog_component__WEBPACK_IMPORTED_MODULE_17__["LogoutdialogComponent"]
            ],
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/app.service.ts":
/*!********************************!*\
  !*** ./src/app/app.service.ts ***!
  \********************************/
/*! exports provided: Foo, AppService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Foo", function() { return Foo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppService", function() { return AppService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var Foo = /** @class */ (function () {
    function Foo(id, email) {
        this.id = id;
        this.email = email;
    }
    return Foo;
}());

var AppService = /** @class */ (function () {
    function AppService(_router, _http) {
        this._router = _router;
        this._http = _http;
    }
    AppService.prototype.obtainAccessToken = function (loginData) {
        var _this = this;
        var params = new URLSearchParams();
        params.append('username', loginData.username);
        params.append('password', loginData.password);
        params.append('grant_type', 'password');
        params.append('client_id', 'smartfarm-client');
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({ 'Content-type': 'application/x-www-form-urlencoded; charset=utf-8',
            'Authorization': 'Basic ' + btoa("smartfarm-client:smartfarm-password-client") });
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({ headers: headers });
        this._http.post(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].contextPathServer + '/oauth/token', params.toString(), options)
            .subscribe(function (rez) { return _this.saveToken(rez.json()); }, function (err) {
            console.log('AUTHENTICATION ERROR: ', err);
        });
    };
    AppService.prototype.saveToken = function (token) {
        var expireDate = new Date().getTime() + (1000 * token.expires_in);
        // Cookie.set("access_token", token.access_token, expireDate);
        console.log('TOKEN FROM SERVER: ', token);
        localStorage.setItem("access_token", token.access_token);
        localStorage.setItem('loggedin', 'yes');
        this._router.navigate(['/']);
        // location.reload();
    };
    AppService.prototype.getResource = function (resourceUrl) {
        var headers = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["Headers"]({ 'Content-type': 'application/x-www-form-urlencoded; charset=utf-8',
            'Authorization': 'Bearer ' + localStorage.getItem('access_token') });
        var options = new _angular_http__WEBPACK_IMPORTED_MODULE_2__["RequestOptions"]({ headers: headers });
        // .map((res:Response) => res.json())
        //.catch((error:any) => Observable.throw(error.json().error || 'Server error'));
        return this._http.get(resourceUrl, options)
            .subscribe(function (rez) {
            console.log('Got resource from REST smartfarm server: ', rez);
        }, function (err) {
            console.log('COULD NOT LOAD RESOURCE');
        });
    };
    AppService.prototype.checkCredentials = function () {
        if (!localStorage.getItem('access_token')) {
            this._router.navigate(['/login']);
        }
    };
    AppService.prototype.logout = function () {
        localStorage.removeItem('access_token');
        this._router.navigate(['/login']);
    };
    AppService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _angular_http__WEBPACK_IMPORTED_MODULE_2__["Http"]])
    ], AppService);
    return AppService;
}());



/***/ }),

/***/ "./src/app/farms/farms-details/farms-details.component.css":
/*!*****************************************************************!*\
  !*** ./src/app/farms/farms-details/farms-details.component.css ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/farms/farms-details/farms-details.component.html":
/*!******************************************************************!*\
  !*** ./src/app/farms/farms-details/farms-details.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\n  \n  <div *ngIf=\"currentFarm\">\n    <app-farms-view [ferma]=\"currentFarm\" ></app-farms-view>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/farms/farms-details/farms-details.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/farms/farms-details/farms-details.component.ts ***!
  \****************************************************************/
/*! exports provided: FarmsDetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FarmsDetailsComponent", function() { return FarmsDetailsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _farmsrv_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../farmsrv.service */ "./src/app/farms/farmsrv.service.ts");
/* harmony import */ var _geoloc_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../geoloc.service */ "./src/app/farms/geoloc.service.ts");
/* harmony import */ var _logout_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../logout.service */ "./src/app/logout.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var FarmsDetailsComponent = /** @class */ (function () {
    function FarmsDetailsComponent(route, service, geoloc, logoutService) {
        this.route = route;
        this.service = service;
        this.geoloc = geoloc;
        this.logoutService = logoutService;
        this.currentFarm = null;
    }
    FarmsDetailsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.currentFarmId = params['idfarm'];
            _this.service.findFarmById(_this.currentFarmId).subscribe(function (rez) {
                console.log('loaded all farms: ', rez.json());
                _this.currentFarm = rez.json();
                var f = _this.currentFarm;
                if (_this.currentFarm.address) {
                    _this.geoloc.getLatLongFromAddress(f.address).subscribe(function (res) {
                        console.log('GEOLOC RESULTS: ', res.json());
                        var latLong = res.json().results[0].geometry;
                        console.log('LATLONG: ', latLong);
                        f.coord = latLong;
                    }, function (err) {
                        console.log('GEOLOC ERROR: ', err);
                    });
                }
            }, function (err) {
                console.log('ERROR STATUS: ', err.status);
                if (err.status == 401) {
                    // which will result in the user being redirected to the
                    // login component (via router)
                    _this.logoutService.openDialog();
                }
                else if (err.status == 403) {
                    // resource is not accessible - farm does not belong to the currently logged in user
                    // alert('You cannot view this farm')
                    _this.logoutService.openDialog("Private Farm Profile", "You cannot view this farm's details", "OK");
                }
            });
        });
    };
    FarmsDetailsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-farms-details',
            template: __webpack_require__(/*! ./farms-details.component.html */ "./src/app/farms/farms-details/farms-details.component.html"),
            styles: [__webpack_require__(/*! ./farms-details.component.css */ "./src/app/farms/farms-details/farms-details.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"], _farmsrv_service__WEBPACK_IMPORTED_MODULE_2__["FarmsrvService"], _geoloc_service__WEBPACK_IMPORTED_MODULE_3__["GeolocService"],
            _logout_service__WEBPACK_IMPORTED_MODULE_4__["LogoutService"]])
    ], FarmsDetailsComponent);
    return FarmsDetailsComponent;
}());



/***/ }),

/***/ "./src/app/farms/farms-list/farms-list.component.css":
/*!***********************************************************!*\
  !*** ./src/app/farms/farms-list/farms-list.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/farms/farms-list/farms-list.component.html":
/*!************************************************************!*\
  !*** ./src/app/farms/farms-list/farms-list.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <button mat-button (click)=\"openDialog()\">Open dialog</button> -->\n<div class=\"table-responsive\">\n  <table class=\"table table-bordered\" id=\"dataTable\" width=\"100%\" cellspacing=\"0\">\n    <thead>\n      <tr>\n        <th>ID</th>\n        <th>Farm Name</th>\n        <th>Details</th>\n        <th>Delete</th>\n      </tr>\n    </thead>\n    <tfoot>\n      <tr>\n        <th>ID</th>\n        <th>Farm Name</th>\n        <th>Details</th>\n        <th>Delete</th>\n      </tr>\n    </tfoot>\n    <tbody>\n      <tr *ngFor=\"let f of farms\">\n        <td>{{f.id}}</td>\n        <td>{{f.farmName}}</td>\n        <td>\n          <a [routerLink]=\"['/farms', f.id]\" routerLinkActive=\"active\">Details</a>\n        </td>\n        <td>\n          <button (click)=\"deleteFarm(f.id)\" class=\"btn btn-danger\">Delete</button>\n        </td>\n\n      </tr>\n\n    </tbody>\n  </table>\n</div>\n\n<div>\n  <h2>Add farm</h2>\n  <h3>Address</h3>\n\n\n  <div class=\"form-group\">\n    <label for=\"city\">City</label>\n    <input style=\"width: 50%;\" [(ngModel)]=\"city\" class=\"form-control\" id=\"city\" placeholder=\"City\">\n  </div>\n\n  <div class=\"form-group\">\n    <label for=\"street\">Street</label>\n    <input style=\"width: 50%;\" [(ngModel)]=\"street\" class=\"form-control\" id=\"street\" placeholder=\"Street\">\n  </div>\n\n\n  <div class=\"form-group\">\n    <label for=\"number\">Number</label>\n    <input style=\"width: 50%;\" [(ngModel)]=\"number\" class=\"form-control\" id=\"number\" placeholder=\"Number\">\n  </div>\n\n  <h3>\n    Farm information\n  </h3>\n\n  <div class=\"form-group\">\n      <label for=\"farm-name\">Farm name</label>\n      <input style=\"width: 50%;\" [(ngModel)]=\"farmName\" class=\"form-control\" id=\"farm-name\" placeholder=\"Farm name\">\n    </div>\n    \n</div>\n<button (click)=\"savefarm(city, street, number, farmName)\">Save farm</button>\n\n<div>\n  <!-- <h2>All farms here</h2> -->\n\n\n  <!-- <div *ngFor=\"let f of farms\" style=\"border: 1px solid black;\">\n      LINK FERMA DETAILS: <a [routerLink]=\"['/farms', f.id]\" routerLinkActive=\"active\">LINK TEST</a>\n      <br>\n     \n    </div> -->\n\n</div>"

/***/ }),

/***/ "./src/app/farms/farms-list/farms-list.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/farms/farms-list/farms-list.component.ts ***!
  \**********************************************************/
/*! exports provided: FarmsListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FarmsListComponent", function() { return FarmsListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _farmsrv_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../farmsrv.service */ "./src/app/farms/farmsrv.service.ts");
/* harmony import */ var _geoloc_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../geoloc.service */ "./src/app/farms/geoloc.service.ts");
/* harmony import */ var _logout_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../logout.service */ "./src/app/logout.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var FarmsListComponent = /** @class */ (function () {
    function FarmsListComponent(farmService, geoloc, logoutService) {
        this.farmService = farmService;
        this.geoloc = geoloc;
        this.logoutService = logoutService;
        this.farms = [];
        this.city = '';
        this.street = '';
        this.number = '';
        this.farmName = '';
    }
    FarmsListComponent.prototype.loadFarms = function () {
        var _this = this;
        this.farmService.findAllFarms().subscribe(function (rez) {
            console.log('loaded all farms: ', rez.json());
            _this.farms = rez.json();
            var _loop_1 = function (f) {
                console.log('FERMA === ', f);
                if (f.address) {
                    _this.geoloc.getLatLongFromAddress(f.address).subscribe(function (res) {
                        console.log('GEOLOC RESULTS: ', res.json());
                        var latLong = res.json().results[0].geometry;
                        console.log('LATLONG: ', latLong);
                        f.coord = latLong;
                    }, function (err) {
                        console.log('GEOLOC ERROR: ', err);
                    });
                }
            };
            for (var _i = 0, _a = _this.farms; _i < _a.length; _i++) {
                var f = _a[_i];
                _loop_1(f);
            }
        }, function (err) {
            console.log('ERROR STATUS: ', err.status);
            if (err.status == 401) {
                _this.logoutService.openDialog();
            }
        });
    };
    FarmsListComponent.prototype.ngOnInit = function () {
        this.loadFarms();
    };
    FarmsListComponent.prototype.deleteFarm = function (farmId) {
        var _this = this;
        this.farmService.deleteFarmById(farmId).subscribe(function (rez) {
            _this.loadFarms();
        }, function (err) {
            console.log('error: ', err);
        });
    };
    FarmsListComponent.prototype.savefarm = function (city, street, pnumber, farmName) {
        var _this = this;
        var jsonFarm = {
            city: city,
            street: street,
            number: pnumber,
            farmName: farmName
        };
        this.farmService.saveFarm(jsonFarm).subscribe(function (rez) {
            console.log('saved farm: ', rez.json());
            _this.loadFarms();
            _this.street = '';
            _this.city = '';
            _this.number = '';
            _this.farmName = '';
        }, function (err) {
            console.log('could not save farm', err);
        });
    };
    FarmsListComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-farms-list',
            template: __webpack_require__(/*! ./farms-list.component.html */ "./src/app/farms/farms-list/farms-list.component.html"),
            styles: [__webpack_require__(/*! ./farms-list.component.css */ "./src/app/farms/farms-list/farms-list.component.css")]
        }),
        __metadata("design:paramtypes", [_farmsrv_service__WEBPACK_IMPORTED_MODULE_1__["FarmsrvService"], _geoloc_service__WEBPACK_IMPORTED_MODULE_2__["GeolocService"],
            _logout_service__WEBPACK_IMPORTED_MODULE_3__["LogoutService"]])
    ], FarmsListComponent);
    return FarmsListComponent;
}());



/***/ }),

/***/ "./src/app/farms/farms-sector-details/farms-sector-details.component.css":
/*!*******************************************************************************!*\
  !*** ./src/app/farms/farms-sector-details/farms-sector-details.component.css ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "input.short-input{\r\n    width: 70%;\r\n}"

/***/ }),

/***/ "./src/app/farms/farms-sector-details/farms-sector-details.component.html":
/*!********************************************************************************!*\
  !*** ./src/app/farms/farms-sector-details/farms-sector-details.component.html ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- -------------------- sector details -------------------- -->\n\n<h2>FORM ADD SECTOR DETAIL</h2>\n<br> Application date:\n\n<mat-form-field>\n  <input matInput [matDatepicker]=\"picker\" placeholder=\"Application date\" [(ngModel)]=\"sector.applicationDate\">\n  <mat-datepicker-toggle matSuffix [for]=\"picker\"></mat-datepicker-toggle>\n  <mat-datepicker #picker></mat-datepicker>\n</mat-form-field>\n\n<div>\n\n  <mat-checkbox [(ngModel)]=\"shouldChooseTime\" >Choose time</mat-checkbox>\n  <div *ngIf=\"shouldChooseTime\">\n    Hour:\n    <input [(ngModel)]=\"selectedHour\">\n    <br> Minute:\n    <input [(ngModel)]=\"selectedMinute\">\n    <br>\n  </div>\n</div>\n\n<!-- <input class=\"application-date-field\" [(ngModel)]=\"sector.applicationDate\"> -->\n<br>\n<div class=\"form-group\">\n  <label for=\"inputPropertyName\">Property name</label>\n  <input style=\"width: 50%;\" [(ngModel)]=\"sector.propertyName\" class=\"form-control\" id=\"inputPropertyName\" placeholder=\"Property Name\">\n</div>\n\n<div class=\"form-group\">\n  <label for=\"inputPropertyValue\">Property value</label>\n  <input style=\"width: 50%;\" [(ngModel)]=\"sector.propertyValue\" class=\"form-control\" id=\"inputPropertyValue\" placeholder=\"Property Value\">\n</div>\n\n\n\n<br>\n\n<div class=\"form-group\">\n  <label for=\"inputSelectOperation\">Select operation</label>\n  <select [(ngModel)]=\"sector.selectedOperation\" style=\"width: 50%;\" class=\"form-control\" id=\"inputSelectOperation\">\n    <option *ngFor=\"let op of operations\" [ngValue]=\"op\">{{op.operationName}}</option>\n  </select>\n</div>\n\n\n\n<span *ngIf=\"sector.selectedOperation\">Ai selectat operatiunea: {{sector.selectedOperation.operationName}}</span>\n<br>\n\n<button class=\"btn btn-info\" (click)=\"addSectorDetail(sector)\">Add Sector detail</button>\n\n<h3>Sector details:</h3>\n\n\n<h4>Last applied operations</h4>\n<div *ngFor=\"let lastOp of lastOperationPropertyByCategoryArray\">\n  <span><strong>{{lastOp.farmOperation.operationName}} </strong></span> - {{lastOp.dateApplied | date : 'yyyy-MM-dd HH:mm'}} \n  <div *ngIf=\"lastOp.expirationTimer\">\n    <span>Operation performed {{lastOp.expirationTimer}} minutes ago</span>\n    <br>\n    <span>Operation interval {{lastOp.correspondingInterval.interval.interval}} minutes</span> \n    <span style=\"color: #FF0000;\" *ngIf=\"lastOp.expirationTimer > lastOp.correspondingInterval.interval.interval\">\n      - Interval for performing this operation has expired\n    </span>\n\n  </div>\n</div>\n\n\nItems per page:\n<input [(ngModel)]=\"ipp\">\n<br>\n<pagination-controls (pageChange)=\"pageChange($event)\"></pagination-controls>\n<div class=\"table-responsive\">\n  <table class=\"table table-bordered\" id=\"dataTable\" width=\"100%\" cellspacing=\"0\">\n    <thead>\n      <tr>\n        <th>Sector details ID</th>\n        <th>Date</th>\n        <th>ID (op)</th>\n        <th>Name</th>\n        <th>Value</th>\n        <th>Category</th>\n        <th>Actions Operation</th>\n        <th>Delete SD</th>\n        <th>Update SD</th>\n      </tr>\n    </thead>\n\n\n\n\n    <tbody>\n      <tr *ngFor=\"let d of sector.farmSectorDetails | paginate : { currentPage : currentPage,  itemsPerPage: ipp}\">\n\n        <td>\n          {{d.id}}\n        </td>\n        <td>\n          <mat-form-field>\n            <input matInput [matDatepicker]=\"picker\" placeholder=\"Application date\" [(ngModel)]=\"d.applicationDate\">\n            <mat-datepicker-toggle matSuffix [for]=\"picker\"></mat-datepicker-toggle>\n            <mat-datepicker #picker></mat-datepicker>\n          </mat-form-field>\n\n          <!-- {{d.applicationDate | date: 'yyyy/MM/dd' }}\n              <input [(ngModel)]=\"d.applicationDate\"> -->\n        </td>\n\n\n\n        <!-- display existing operation property -->\n        <td *ngIf=\"d.farmOperationProperty\">id: {{d.farmOperationProperty.id}}</td>\n        <td *ngIf=\"d.farmOperationProperty\">\n          <input class=\"short-input\" [(ngModel)]=\"d.farmOperationProperty.propertyName\">\n        </td>\n        <td *ngIf=\"d.farmOperationProperty\">\n\n\n          <input class=\"short-input\" [(ngModel)]=\"d.farmOperationProperty.propertyValue\">\n        </td>\n\n        <td *ngIf=\"d.farmOperationProperty\"> {{d.farmOperationProperty.farmOperation.operationName}}\n\n          <select [(ngModel)]=\"d.farmOperationProperty.farmOperation\">\n            <option *ngFor=\"let op of operations\" [ngValue]=\"op\">{{op.operationName}}</option>\n          </select>\n\n          <a *ngIf=\"d.farmOperationProperty.farmOperation.operationName == 'irigare'\" [routerLink]=\"['/irigation', d.farmOperationProperty.id]\">View report</a>\n        </td>\n\n        <td *ngIf=\"d.farmOperationProperty\">\n          <button class=\"btn btn-danger\" (click)=\"deleteOperationProperty(d.farmOperationProperty, d.id, sector)\">\n            Delete\n          </button>\n        </td>\n\n\n\n\n        <!-- Add operation property -->\n        <td *ngIf=\"!d.farmOperationProperty\">ID N/A</td>\n        <td *ngIf=\"!d.farmOperationProperty\">\n          <input class=\"short-input\" [(ngModel)]=\"d.propertyName\">\n        </td>\n        <td *ngIf=\"!d.farmOperationProperty\">\n          <input class=\"short-input\" [(ngModel)]=\"d.propertyValue\">\n        </td>\n        <td *ngIf=\"!d.farmOperationProperty\">\n          <select [(ngModel)]=\"d.selectedOperation\">\n            <option *ngFor=\"let op of operations\" [ngValue]=\"op\">{{op.operationName}}</option>\n          </select>\n        </td>\n        <td *ngIf=\"!d.farmOperationProperty\">\n          <button (click)=\"addOPToSectorDetail(sector.id, d.id,  d.propertyName, d.propertyValue, d.selectedOperation)\">\n            Add\n\n          </button>\n        </td>\n\n\n\n        <td>\n          <button class=\"btn btn-danger\" (click)=\"deleteSectorDetails(d)\">Del SD</button>\n\n        </td>\n        <td>\n          <button class=\"btn btn-warning\" (click)=\"updateSectorDetails(sector.id, d)\">Update SD</button>\n        </td>\n\n      </tr>\n\n    </tbody>\n  </table>\n</div>"

/***/ }),

/***/ "./src/app/farms/farms-sector-details/farms-sector-details.component.ts":
/*!******************************************************************************!*\
  !*** ./src/app/farms/farms-sector-details/farms-sector-details.component.ts ***!
  \******************************************************************************/
/*! exports provided: YYYY_MM_DD_Format, FarmsSectorDetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "YYYY_MM_DD_Format", function() { return YYYY_MM_DD_Format; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FarmsSectorDetailsComponent", function() { return FarmsSectorDetailsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _farmsrv_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../farmsrv.service */ "./src/app/farms/farmsrv.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _logout_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../logout.service */ "./src/app/logout.service.ts");
/* harmony import */ var _sectorinterval_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../sectorinterval.service */ "./src/app/farms/sectorinterval.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var YYYY_MM_DD_Format = {
    parse: {
        dateInput: 'DD-MM-YYYY',
    },
    display: {
        dateInput: 'DD-MM-YYYY',
        monthYearLabel: 'MMM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MMMM YYYY',
    },
};
var FarmsSectorDetailsComponent = /** @class */ (function () {
    function FarmsSectorDetailsComponent(serviceFarm, logoutService, intervalService) {
        this.serviceFarm = serviceFarm;
        this.logoutService = logoutService;
        this.intervalService = intervalService;
        this.ipp = 5;
        this.lastOperationPropertyByCategory = {};
        this.lastOperationPropertyByCategoryArray = [];
        this.intervalDtos = [];
        this.evenimentSectorDetailsComponent = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.currentPage = 1;
        this.operations = [];
        this.operationIdAndName = {};
    }
    // @Output() eveniment : EventEmitter<string> = new EventEmitter();
    FarmsSectorDetailsComponent.prototype.ngOnChanges = function (changes) {
        console.log('FARM SECTOR DETAILS COMPONENT CHANGE');
    };
    FarmsSectorDetailsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.intervalService.findIntervalsForSector(this.sector.id).subscribe(function (rez) {
            _this.intervalDtos = rez;
            console.log('successfully loaded intervals: ', _this.intervalDtos);
            _this.serviceFarm.findAllOperations().subscribe(function (rez) {
                _this.operations = rez.json();
                console.log('EXISTING OPERATIONS: ', _this.operations);
                for (var _i = 0, _a = _this.operations; _i < _a.length; _i++) {
                    var operationType = _a[_i];
                    _this.lastOperationPropertyByCategory[operationType.id] = [];
                    _this.operationIdAndName[operationType.id] = operationType.operationName;
                }
                // populate lastOperationByCategory array with the values from the already loaded sector
                for (var _b = 0, _c = _this.sector.farmSectorDetails; _b < _c.length; _b++) {
                    var fsd = _c[_b];
                    if (fsd.farmOperationProperty) {
                        var operationId = fsd.farmOperationProperty.farmOperation.id;
                        _this.lastOperationPropertyByCategory[operationId].push(fsd.farmOperationProperty);
                    }
                }
                for (var _d = 0, _e = Object.keys(_this.lastOperationPropertyByCategory); _d < _e.length; _d++) {
                    var idCategory = _e[_d];
                    var lastOperationCategory = _this.lastOperationPropertyByCategory[idCategory];
                    for (var _f = 0, lastOperationCategory_1 = lastOperationCategory; _f < lastOperationCategory_1.length; _f++) {
                        var opProp = lastOperationCategory_1[_f];
                        opProp.dateApplied = new Date(opProp.dateApplied);
                        console.log('DATE APPLIED TEST: ', opProp.dateApplied.getTime());
                    }
                    lastOperationCategory = lastOperationCategory.sort(function (x, y) { return y.dateApplied.getTime() - x.dateApplied.getTime(); });
                }
                // lastOperationPropertyByCategory contains all the operation properties sorted by date (desc)
                // only the firt element of the array (should it exist), should be added
                // in order to compare with the interval
                for (var _g = 0, _h = Object.keys(_this.lastOperationPropertyByCategory); _g < _h.length; _g++) {
                    var idOpCat = _h[_g];
                    var opCat = _this.lastOperationPropertyByCategory[idOpCat];
                    console.log('OP CAT: ', opCat);
                    if (opCat.length > 0) {
                        _this.lastOperationPropertyByCategory[idOpCat] = opCat[0];
                        var now = new Date();
                        var dateApplied = opCat[0].dateApplied;
                        // console.log('DIFFERENCE: ', (now.getTime() - dateApplied.getTime()));
                        var diff = now.getTime() - dateApplied.getTime();
                        // console.log('diff for operation:  ' + opCat[0] + ' is : ' + diff);
                        if (diff < 0) {
                            opCat[0].expirationTimer = null;
                        }
                        else {
                            var correspondingInterval = null;
                            for (var _j = 0, _k = _this.intervalDtos; _j < _k.length; _j++) {
                                var intervalS = _k[_j];
                                if (intervalS.operation.id == opCat[0].farmOperation.id) {
                                    correspondingInterval = intervalS;
                                    opCat[0].correspondingInterval = correspondingInterval;
                                    break;
                                }
                            }
                            if (correspondingInterval != null) {
                                opCat[0].expirationTimer = diff / (1000 * 60);
                            }
                            else {
                                opCat[0].expirationTimer = null; // operations which do not have an associated interval (e.g. seeding)
                            }
                        }
                        _this.lastOperationPropertyByCategoryArray.push(opCat[0]);
                    }
                    else {
                        _this.lastOperationPropertyByCategory[idOpCat] = null;
                    }
                }
                console.log('LAST OPERATION ARRAY: ', _this.lastOperationPropertyByCategoryArray);
            }, function (err) {
                console.log('ERROR STATUS: ', err.status);
                if (err.status == 401) {
                    _this.logoutService.openDialog();
                }
            });
        }, function (err) {
            console.log('could not load intervals', err);
        });
    };
    FarmsSectorDetailsComponent.prototype.addSectorDetail = function (sector) {
        var _this = this;
        var applicationDate = sector.applicationDate;
        var propertyName = sector.propertyName;
        var propertyValue = sector.propertyValue;
        var farmOperation = sector.selectedOperation;
        if (this.shouldChooseTime) {
            applicationDate.setHours(this.selectedHour, this.selectedMinute, 0);
        }
        else {
            applicationDate.setHours(0, 0, 0);
        }
        var json = {
            applicationDate: applicationDate,
            farmOperationProperty: {
                propertyName: propertyName,
                propertyValue: propertyValue,
                farmOperation: farmOperation
            }
        };
        this.serviceFarm.saveFarmSectorDetail(sector.id, json).subscribe(function (rez) {
            console.log('salvarea farm sector detail fu ok');
            console.log(rez.json());
            _this.evenimentSectorDetailsComponent.emit('ADDED SECTOR DETAIL (operation included)');
        }, function (err) {
            console.log('eroare salvare farm sector detail', err);
        });
    };
    /**
      *
      * @param sectorId
      * @param sd sectorDetails object to update
      */
    FarmsSectorDetailsComponent.prototype.updateSectorDetails = function (sectorId, sd) {
        var _this = this;
        console.log('***sector: ' + sectorId);
        console.log('updating sd: ', sd);
        this.serviceFarm.updateSectorDetails(sectorId, sd).subscribe(function (rez) {
            console.log('successfully updated, RESPONSE: ', rez.json());
            _this.evenimentSectorDetailsComponent.emit('SECTOR DETAILS - UPDATE');
        }, function (err) {
            console.log('failure to update: ', err);
        });
    };
    FarmsSectorDetailsComponent.prototype.refreshFarmSectorDetails = function () {
        this.evenimentSectorDetailsComponent.emit('info child');
    };
    FarmsSectorDetailsComponent.prototype.addOPToSectorDetail = function (farmSectorId, farmSectorDetailId, propertyName, propertyValue, selectedOperation) {
        var _this = this;
        console.log("PROPERTY NAME: " + propertyName + " PROPERTY VALUE: " + propertyValue + " SELECTED OPERATION: ", selectedOperation);
        this.serviceFarm.saveFarmOperationPropertyToFarmSector(farmSectorId, farmSectorDetailId, propertyName, propertyValue, selectedOperation).subscribe(function (rez) {
            console.log('SAVE OK', rez.json());
            _this.evenimentSectorDetailsComponent.emit('SECTOR DETAILS - ADD OP TO SECTOR DETAIL');
        }, function (err) {
            console.log('SAVE FAILED', err);
        });
    };
    FarmsSectorDetailsComponent.prototype.deleteSectorDetails = function (detail) {
        var _this = this;
        this.serviceFarm.deleteSectorDetail(detail.id).subscribe(function (rez) {
            console.log('saved new sector', rez.json());
            // this.sectoare = rez.json();
            //this.refreshFarmSectorDetails();
            _this.evenimentSectorDetailsComponent.emit('SECTOR DETAILS - DELETE');
        }, function (err) {
            console.log('error: ', err);
        });
    };
    FarmsSectorDetailsComponent.prototype.deleteOperationProperty = function (farmOperationProperty, sectorId, sector) {
        var _this = this;
        console.log('deleting FOP: ', farmOperationProperty);
        this.serviceFarm.unbindOperationDetail(farmOperationProperty.id, sectorId).subscribe(function (rez) {
            console.log('successful unbound OP', rez.json());
            _this.serviceFarm.deleteOperationDetail(farmOperationProperty.id, sectorId).subscribe(function (rezDelete) {
                console.log('successful delete OP', rezDelete.json());
                // this.reloadSectors();
                _this.evenimentSectorDetailsComponent.emit('SECTOR DETAILS - DELETE OPERATION PROPERTY FOR SECTOR DETAIL (2)');
                // sector.farmSectorProperties.splice(sector.farmSectorProperties.indexOf(farmOperationProperty), 1);
            }, function (errDelete) {
                console.log('error delete OP: ', errDelete);
            });
        }, function (err) {
            console.log('error unbind: ', err);
        });
    };
    FarmsSectorDetailsComponent.prototype.pageChange = function (event) {
        console.log('pg number: ', event);
        this.currentPage = event;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], FarmsSectorDetailsComponent.prototype, "sector", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"])
    ], FarmsSectorDetailsComponent.prototype, "evenimentSectorDetailsComponent", void 0);
    FarmsSectorDetailsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-farms-sector-details',
            template: __webpack_require__(/*! ./farms-sector-details.component.html */ "./src/app/farms/farms-sector-details/farms-sector-details.component.html"),
            styles: [__webpack_require__(/*! ./farms-sector-details.component.css */ "./src/app/farms/farms-sector-details/farms-sector-details.component.css")],
            providers: [
                { provide: _angular_material__WEBPACK_IMPORTED_MODULE_2__["MAT_DATE_LOCALE"], useValue: YYYY_MM_DD_Format }
            ]
        }),
        __metadata("design:paramtypes", [_farmsrv_service__WEBPACK_IMPORTED_MODULE_1__["FarmsrvService"], _logout_service__WEBPACK_IMPORTED_MODULE_3__["LogoutService"],
            _sectorinterval_service__WEBPACK_IMPORTED_MODULE_4__["SectorintervalService"]])
    ], FarmsSectorDetailsComponent);
    return FarmsSectorDetailsComponent;
}());



/***/ }),

/***/ "./src/app/farms/farms-view/farms-view.component.css":
/*!***********************************************************!*\
  !*** ./src/app/farms/farms-view/farms-view.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "agm-map {\r\n    height: 300px;\r\n  }"

/***/ }),

/***/ "./src/app/farms/farms-view/farms-view.component.html":
/*!************************************************************!*\
  !*** ./src/app/farms/farms-view/farms-view.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\n  ID: {{ferma.id}}\n  <br> Name: {{ferma.farmName}}\n\n\n  <div *ngIf=\"ferma.address; else nomaps\">\n\n    <p *ngIf=\"ferma.coord\">\n      LATITUDINE: {{ferma.coord.lat}}\n      <br> LONGITUDINE: {{ferma.coord.lng}}\n      <br>\n\n      <agm-map [draggable]= \"false\" [panControl]= \"false\"  [zoom] = \"16\"  [minZoom] = \"6\" [latitude]=\"ferma.coord.lat\" [longitude]=\"ferma.coord.lng\">\n        <agm-marker [latitude]=\"ferma.coord.lat\" [longitude]=\"ferma.coord.lng\"></agm-marker>\n      </agm-map>\n    </p>\n\n\n  </div>\n  <ng-template #nomaps>\n    <span style=\"color: red;\">Cannot render map for missing address</span>\n  </ng-template>\n\n  <div *ngIf=\"ferma.address ; else addAddress\">\n    <div *ngIf=\"!editAddress; else editAddressDiv\">\n      Adresa: {{ferma.address.id}} - {{ferma.address.city}} {{ferma.address.street}} {{ferma.address.sNumber}} City:\n      <button class=\"btn btn-info\"  (click)=\"editAddress = !editAddress\">Edit / Update</button>\n    </div>\n    <ng-template #editAddressDiv>\n\n\n\n      <div class=\"form-group\">\n        <label for=\"inputAddressCity\">City</label>\n        <input [(ngModel)]=\"ferma.address.city\" style=\"width: 50%;\" class=\"form-control\" id=\"inputAddressCity\" placeholder=\"City\">\n      </div>\n\n      <div class=\"form-group\">\n          <label for=\"inputAddressStreet\">Street</label>\n          <input [(ngModel)]=\"ferma.address.street\" style=\"width: 50%;\" class=\"form-control\" id=\"inputAddressStreet\" placeholder=\"Street\">\n        </div>\n\n      <div class=\"form-group\">\n          <label for=\"inputAddressNumber\">Number</label>\n          <input [(ngModel)]=\"ferma.address.sNumber\" style=\"width: 50%;\" class=\"form-control\" id=\"inputAddressNumber\" placeholder=\"Number\">\n        </div>\n\n        \n\n\n      <!-- <br> Street:\n      <input [(ngModel)]=\"ferma.address.street\">\n      <br> Number\n      <input [(ngModel)]=\"ferma.address.sNumber\">\n      <br> -->\n      <button class=\"btn btn-info\"  (click)=\"editAddress = !editAddress\">Cancel</button>\n      <button  class=\"btn btn-danger\" (click)=\"updateAddress(ferma.address)\">Update address</button>\n    </ng-template>\n\n  </div>\n\n\n  <ng-template #addAddress>\n    <span style=\"color: red;\"> No address defined for this farm </span>\n    <br> City:\n    <input [(ngModel)]=\"address.city\">\n    <br> Street:\n    <input [(ngModel)]=\"address.street\">\n    <br> Number\n    <input [(ngModel)]=\"address.sNumber\">\n    <br>\n\n    <button (click)=\"saveAddress(address, ferma.id)\">Save address</button>\n  </ng-template>\n\n\n  \n  <h3>Add new sector</h3>\n\n  <div class=\"form-group\">\n    <label for=\"inputSectorDescription\">Sector description</label>\n    <input style=\"width: 50%;\" [(ngModel)]=\"newSector.sectorDescription\" class=\"form-control\" id=\"inputSectorDescription\" placeholder=\"Description\">\n  </div>\n\n  <div class=\"form-group\">\n    <label for=\"inputSectorSurface\">Sector surface</label>\n    <input style=\"width: 50%;\" [(ngModel)]=\"newSector.surface\" class=\"form-control\" id=\"inputSectorSurface\" placeholder=\"Surface\">\n  </div>\n\n  <div class=\"form-group\">\n\n      <label for=\"inputSoilType\">Soil type</label>\n  <select [(ngModel)]=\"newSector.soilTypeBean\" id=\"inputSoilType\">\n      <option *ngFor=\"let st of soilTypes\" [ngValue]=\"st\">{{st.name}}</option>\n  </select>\n  </div>\n\n\n  <button (click)=\"saveNewSector($event);\" class=\"btn btn-info\">Save sector</button>\n\n\n  <h3 style=\"padding-top: 10px;\">Sectoare</h3>\n\n  <div class=\"table-responsive\">\n    <table class=\"table table-bordered\" id=\"dataTable\" width=\"100%\" cellspacing=\"0\">\n      <thead>\n        <tr>\n          <th>ID</th>\n          <th>Sector Description</th>\n          <th>Surface</th>\n          <th>Map</th>\n          <th>Details</th>\n          <th>Delete</th>\n        </tr>\n      </thead>\n      <tfoot>\n        <tr>\n          <th>ID</th>\n          <th>Sector Description</th>\n          <th>Surface</th>\n          <th>Map</th>\n          <th>Details</th>\n          <th>Delete</th>\n        </tr>\n      </tfoot>\n      <tbody>\n        <tr *ngFor=\"let sector of sectoare\">\n          <td>{{sector.id}}</td>\n          <td>{{sector.sectorDescription}}</td>\n          <td>{{sector.surface}}</td>\n          <td>\n            <a class=\"btn btn-info\" [routerLink]=\"['/sector-map', sector.id]\">Sector Map</a>\n          </td>\n          <td>\n            <a [routerLink]=\"['/farms/sectors/', sector.id]\">Details</a>\n          </td>\n          <td>\n            <button class=\"btn btn-danger\" (click)=\"delSector(sector)\">Delete Sector</button>\n          </td>\n\n        </tr>\n\n      </tbody>\n    </table>\n  </div>\n  <!-- <div>\n\n    <div *ngFor=\"let sector of sectoare\">\n      <a [routerLink]=\"['/farms/sectors/', sector.id]\">\n        {{sector.sectorDescription}}\n      </a>\n      <a class=\"btn btn-info\" [routerLink]=\"['/sector-map', sector.id]\">Sector Map</a>\n      <button class=\"btn btn-danger\" (click)=\"delSector(sector)\">Delete Sector</button>\n    </div>\n\n  </div> -->"

/***/ }),

/***/ "./src/app/farms/farms-view/farms-view.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/farms/farms-view/farms-view.component.ts ***!
  \**********************************************************/
/*! exports provided: FarmsViewComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FarmsViewComponent", function() { return FarmsViewComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _farmsrv_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../farmsrv.service */ "./src/app/farms/farmsrv.service.ts");
/* harmony import */ var _logout_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../logout.service */ "./src/app/logout.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var FarmsViewComponent = /** @class */ (function () {
    function FarmsViewComponent(serviceFarm, logoutService) {
        this.serviceFarm = serviceFarm;
        this.logoutService = logoutService;
        this.showProps = false;
        this.editAddress = false;
        this.soilTypes = [];
        this.sectoare = [];
        // the new address
        this.address = {};
        this.lat = 51.678418;
        this.lng = 7.809007;
        this.newSector = {
            "sectorDescription": "",
            "surface": 0,
            "soilTypeBean": null
        };
    }
    // refresh sectors for farm
    FarmsViewComponent.prototype.reloadSectors = function () {
        var _this = this;
        this.serviceFarm.findAllSectorsForFarm(this.ferma.id).subscribe(function (rez) {
            console.log('toate sectoarele pentru ferma curenta: ', rez.json());
            _this.sectoare = rez.json();
        }, function (err) {
            console.log('error load farm sectors: ', err);
            console.log('ERROR STATUS: ', err.status);
            if (err.status == 401) {
                _this.logoutService.openDialog();
            }
        });
    };
    FarmsViewComponent.prototype.loadCoordFromAddress = function (address) {
        var _this = this;
        this.serviceFarm.getLatLongFromAddress(address).subscribe(function (rez) {
            console.log('lat long: ', rez.json());
            var jsonRez = rez.json();
            _this.lat = jsonRez.results[0].geometry.lat;
            _this.lng = jsonRez.results[0].geometry.lng;
            console.log('LATITUDE: ' + _this.lat + ' LONGITUDE: ' + _this.lng);
        }, function (err) {
            console.log(err);
        });
    };
    FarmsViewComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.serviceFarm.findAllSoilTypes().subscribe(function (rez) {
            _this.soilTypes = rez.json();
        }, function (err) {
            console.log('could not load soil types: ', err);
        });
        console.log('FERMA CURENTA: ', this.ferma);
        if (this.ferma.address) {
            this.loadCoordFromAddress(this.ferma.address);
        }
        this.reloadSectors();
    };
    FarmsViewComponent.prototype.saveNewSector = function (event) {
        var _this = this;
        event.preventDefault();
        this.serviceFarm.saveNewSectorForFarm(this.ferma.id, this.newSector).subscribe(function (rez) {
            console.log('saved new sector', rez.json());
            _this.sectoare = rez.json();
        }, function (err) {
            console.log('error: ', err);
        });
    };
    FarmsViewComponent.prototype.refreshFarmSectorDetails = function (farmSectorId, newSectors) {
        for (var i = 0; i < this.sectoare.length; i++) {
            var sectorCurent = this.sectoare[i];
            if (sectorCurent.id == farmSectorId) {
                sectorCurent.farmSectorDetails = newSectors;
            }
        }
    };
    FarmsViewComponent.prototype.delSector = function (sector) {
        var _this = this;
        this.serviceFarm.deleteFarmSector(sector.id).subscribe(function (rez) {
            console.log('saved new sector', rez.json());
            _this.sectoare = rez.json();
        }, function (err) {
            console.log('error: ', err);
        });
    };
    FarmsViewComponent.prototype.updateAddress = function (address) {
        console.log('updating address: ', address);
        this.serviceFarm.updateAddress(address).subscribe(function (rez) {
            console.log(rez.json());
            alert('Successfully updated address');
            location.reload();
        }, function (err) {
            alert('could not update address');
            console.log(err);
        });
    };
    FarmsViewComponent.prototype.saveAddress = function (address, fermaId) {
        console.log('saving address: ', address);
        console.log('ferma id: ' + fermaId);
        this.serviceFarm.saveAddress(address, fermaId).subscribe(function (rez) {
            console.log(rez.json());
            // this.logoutService.openDialog("Address saved", "The address has been successfully saved", "OK");
        }, function (err) {
            alert('could not save address');
            console.log(err);
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], FarmsViewComponent.prototype, "ferma", void 0);
    FarmsViewComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-farms-view',
            template: __webpack_require__(/*! ./farms-view.component.html */ "./src/app/farms/farms-view/farms-view.component.html"),
            styles: [__webpack_require__(/*! ./farms-view.component.css */ "./src/app/farms/farms-view/farms-view.component.css")]
        }),
        __metadata("design:paramtypes", [_farmsrv_service__WEBPACK_IMPORTED_MODULE_1__["FarmsrvService"], _logout_service__WEBPACK_IMPORTED_MODULE_2__["LogoutService"]])
    ], FarmsViewComponent);
    return FarmsViewComponent;
}());



/***/ }),

/***/ "./src/app/farms/farms.module.ts":
/*!***************************************!*\
  !*** ./src/app/farms/farms.module.ts ***!
  \***************************************/
/*! exports provided: FarmsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FarmsModule", function() { return FarmsModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _farms_list_farms_list_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./farms-list/farms-list.component */ "./src/app/farms/farms-list/farms-list.component.ts");
/* harmony import */ var _farms_details_farms_details_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./farms-details/farms-details.component */ "./src/app/farms/farms-details/farms-details.component.ts");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _farms_view_farms_view_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./farms-view/farms-view.component */ "./src/app/farms/farms-view/farms-view.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ng2_date_picker__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ng2-date-picker */ "./node_modules/ng2-date-picker/fesm5/ng2-date-picker.js");
/* harmony import */ var ngx_pagination__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-pagination */ "./node_modules/ngx-pagination/dist/ngx-pagination.js");
/* harmony import */ var _farms_sector_details_farms_sector_details_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./farms-sector-details/farms-sector-details.component */ "./src/app/farms/farms-sector-details/farms-sector-details.component.ts");
/* harmony import */ var _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/datepicker */ "./node_modules/@angular/material/esm5/datepicker.es5.js");
/* harmony import */ var _angular_material_core__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/core */ "./node_modules/@angular/material/esm5/core.es5.js");
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material/form-field */ "./node_modules/@angular/material/esm5/form-field.es5.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _agm_core__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @agm/core */ "./node_modules/@agm/core/index.js");
/* harmony import */ var _sector_map_sector_map_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./sector-map/sector-map.component */ "./src/app/farms/sector-map/sector-map.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _sector_sector_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./sector/sector.component */ "./src/app/farms/sector/sector.component.ts");
/* harmony import */ var _reports_sd_properties_reports_sd_properties_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./reports-sd-properties/reports-sd-properties.component */ "./src/app/farms/reports-sd-properties/reports-sd-properties.component.ts");
/* harmony import */ var angular_google_charts__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! angular-google-charts */ "./node_modules/angular-google-charts/fesm5/angular-google-charts.js");
/* harmony import */ var _irigation_irigation_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./irigation/irigation.component */ "./src/app/farms/irigation/irigation.component.ts");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm5/dialog.es5.js");
/* harmony import */ var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @angular/material/checkbox */ "./node_modules/@angular/material/esm5/checkbox.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









 // <-- import the module














var FarmsModule = /** @class */ (function () {
    function FarmsModule() {
    }
    FarmsModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_http__WEBPACK_IMPORTED_MODULE_4__["HttpModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"],
                ng2_date_picker__WEBPACK_IMPORTED_MODULE_7__["DpDatePickerModule"],
                ngx_pagination__WEBPACK_IMPORTED_MODULE_8__["NgxPaginationModule"],
                _agm_core__WEBPACK_IMPORTED_MODULE_15__["AgmCoreModule"].forRoot({
                    apiKey: 'AIzaSyDyJRQ6n7i9c2ncr03yVaiDs6l45B1GTu8'
                }),
                _angular_router__WEBPACK_IMPORTED_MODULE_17__["RouterModule"],
                _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_10__["MatDatepickerModule"],
                _angular_material_form_field__WEBPACK_IMPORTED_MODULE_12__["MatFormFieldModule"],
                _angular_material_core__WEBPACK_IMPORTED_MODULE_11__["MatNativeDateModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_13__["MatInputModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_14__["BrowserAnimationsModule"],
                angular_google_charts__WEBPACK_IMPORTED_MODULE_20__["GoogleChartsModule"].forRoot(),
                _angular_material_dialog__WEBPACK_IMPORTED_MODULE_22__["MatDialogModule"],
                _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_23__["MatCheckboxModule"]
            ],
            exports: [
                _farms_list_farms_list_component__WEBPACK_IMPORTED_MODULE_2__["FarmsListComponent"], _farms_details_farms_details_component__WEBPACK_IMPORTED_MODULE_3__["FarmsDetailsComponent"], _sector_map_sector_map_component__WEBPACK_IMPORTED_MODULE_16__["SectorMapComponent"], _reports_sd_properties_reports_sd_properties_component__WEBPACK_IMPORTED_MODULE_19__["ReportsSdPropertiesComponent"],
                _irigation_irigation_component__WEBPACK_IMPORTED_MODULE_21__["IrigationComponent"],
            ],
            declarations: [_farms_list_farms_list_component__WEBPACK_IMPORTED_MODULE_2__["FarmsListComponent"], _farms_details_farms_details_component__WEBPACK_IMPORTED_MODULE_3__["FarmsDetailsComponent"],
                _farms_view_farms_view_component__WEBPACK_IMPORTED_MODULE_5__["FarmsViewComponent"], _farms_sector_details_farms_sector_details_component__WEBPACK_IMPORTED_MODULE_9__["FarmsSectorDetailsComponent"], _sector_map_sector_map_component__WEBPACK_IMPORTED_MODULE_16__["SectorMapComponent"], _sector_sector_component__WEBPACK_IMPORTED_MODULE_18__["SectorComponent"], _reports_sd_properties_reports_sd_properties_component__WEBPACK_IMPORTED_MODULE_19__["ReportsSdPropertiesComponent"], _irigation_irigation_component__WEBPACK_IMPORTED_MODULE_21__["IrigationComponent"],
            ],
            providers: [],
        })
    ], FarmsModule);
    return FarmsModule;
}());



/***/ }),

/***/ "./src/app/farms/farmsrv.service.ts":
/*!******************************************!*\
  !*** ./src/app/farms/farmsrv.service.ts ***!
  \******************************************/
/*! exports provided: FarmsrvService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FarmsrvService", function() { return FarmsrvService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var FarmsrvService = /** @class */ (function () {
    function FarmsrvService(_http) {
        this._http = _http;
        this.endpointUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].contextPathServer + '/rest/farms';
        this.contextPath = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].contextPathServer + '/rest';
        this.endpointUrlAddress = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].contextPathServer + '/rest/addresses';
    }
    FarmsrvService.prototype.appendToken = function () {
        return "?access_token=" + localStorage.getItem('access_token');
    };
    FarmsrvService.prototype.findAllFarms = function () {
        return this._http.get(this.endpointUrl + "/farm/all" + this.appendToken());
    };
    FarmsrvService.prototype.findFarmById = function (idfarm) {
        return this._http.get(this.endpointUrl + "/farm/byid/" + idfarm + this.appendToken());
    };
    FarmsrvService.prototype.deleteFarmById = function (idfarm) {
        return this._http.delete(this.endpointUrl + "/farm/delete/" + idfarm + this.appendToken());
    };
    FarmsrvService.prototype.findAllOperations = function () {
        return this._http.get(this.contextPath + "/operations/all" + this.appendToken());
    };
    FarmsrvService.prototype.findAllSectors = function () {
        return this._http.get(this.endpointUrl + "/sectors/all" + this.appendToken());
    };
    FarmsrvService.prototype.findSectorById = function (id) {
        return this._http.get(this.contextPath + "/farms/sectors/byid/" + id + this.appendToken());
    };
    FarmsrvService.prototype.findAllSectorsForFarm = function (farmId) {
        return this._http.get(this.endpointUrl + "/sectors/byfarmid/" + farmId + this.appendToken());
    };
    FarmsrvService.prototype.findAllSectorProperties = function (sectorId) {
        return this._http.get(this.endpointUrl + "/sectors/properties/find/" + sectorId + this.appendToken());
    };
    FarmsrvService.prototype.findAllSoilTypes = function () {
        return this._http.get(this.contextPath + "/soils/all" + this.appendToken());
    };
    FarmsrvService.prototype.saveFarm = function (farmJson) {
        return this._http.post(this.endpointUrl + "/save" + this.appendToken(), farmJson);
    };
    FarmsrvService.prototype.saveNewSectorForFarm = function (farmId, farmSector) {
        return this._http.post(this.endpointUrl + "/sectors/add/" + farmId + this.appendToken(), farmSector);
    };
    FarmsrvService.prototype.saveFarmSectorDetail = function (farmSectorId, farmSectorDetail) {
        return this._http.post(this.endpointUrl + "/sectors/details/add/" + farmSectorId + this.appendToken(), farmSectorDetail);
    };
    FarmsrvService.prototype.saveFarmOperationPropertyToFarmSector = function (farmSectorId, farmSectorDetailId, propertyName, propertyValue, farmOperation) {
        return this._http.post(this.endpointUrl + "/sectors/details/operations/" + farmSectorId + "/" + farmSectorDetailId + "/" + propertyName + "/" + propertyValue + this.appendToken(), farmOperation);
    };
    FarmsrvService.prototype.saveFarmSectorProperty = function (farmSectorPropertyDTO) {
        return this._http.post(this.endpointUrl + "/sectors/properties/add" + this.appendToken(), farmSectorPropertyDTO);
    };
    FarmsrvService.prototype.deleteFarmSector = function (farmSectorId) {
        return this._http.delete(this.endpointUrl + "/sectors/remove/" + farmSectorId + this.appendToken());
    };
    FarmsrvService.prototype.deleteSectorProperty = function (farmSectorPropertyId) {
        return this._http.delete(this.endpointUrl + "/sectors/remove/property/" + farmSectorPropertyId + this.appendToken());
    };
    FarmsrvService.prototype.deleteSectorDetail = function (sectorDetailId) {
        return this._http.delete(this.endpointUrl + "/sectors/remove/detail/" + sectorDetailId + this.appendToken());
    };
    FarmsrvService.prototype.deleteOperationDetail = function (opDetail, idsectordetail) {
        return this._http.delete(this.endpointUrl + "/sectors/details/operations/delete/" + opDetail + "/" + idsectordetail + this.appendToken());
    };
    FarmsrvService.prototype.unbindOperationDetail = function (opDetail, idsectordetail) {
        return this._http.delete(this.endpointUrl + "/sectors/details/operations/unbind/" + opDetail + "/" + idsectordetail + this.appendToken());
    };
    FarmsrvService.prototype.updateProperty = function (sectorid, property) {
        property.farmSectorId = sectorid;
        // private Integer farmSectorPropertyId; // for update
        // private Integer farmSectorId;
        // private String propertyName;
        // private String propertyValue;
        // {id: 1, propName: "humidity 2", propValue: "300"}
        var jsonProperty = {
            farmSectorPropertyId: property.id,
            farmSectorId: sectorid,
            propertyName: property.propName,
            propertyValue: property.propValue,
            dateMeasured: property.dateMeasured
        };
        return this._http.put(this.endpointUrl + "/sectors/properties/update" + this.appendToken(), jsonProperty);
    };
    FarmsrvService.prototype.updateSectorDetails = function (sectorId, sectorDetail) {
        return this._http.put(this.endpointUrl + ("/sectors/sectordetails/" + sectorId + this.appendToken()), sectorDetail);
    };
    FarmsrvService.prototype.updateSectorCoordinates = function (sectorId, sectorCoordinates) {
        return this._http.put(this.contextPath + ("/geo/sector/update/" + sectorId + this.appendToken()), sectorCoordinates);
    };
    FarmsrvService.prototype.updateSectorCoordinatesPolygon = function (sectorId, sector) {
        // /rest/farms/sectors/byid/1
        return this._http.put(this.contextPath + ("/farms/sectors/update/" + sectorId + this.appendToken()), sector);
    };
    FarmsrvService.prototype.updateSectorDeleteCoordinates = function (sectorId) {
        // sectors/updatecoordnull/{idsector}
        return this._http.put(this.contextPath + ("/farms/sectors/updatecoordnull/" + sectorId + this.appendToken()), {});
    };
    FarmsrvService.prototype.updateAddress = function (address) {
        return this._http.put(this.endpointUrlAddress + '/update' + this.appendToken(), address);
    };
    FarmsrvService.prototype.saveAddress = function (address, fermaId) {
        return this._http.post(this.endpointUrlAddress + '/save/' + fermaId + this.appendToken(), address);
    };
    // TODO: create geolocation service
    FarmsrvService.prototype.getLatLongFromAddress = function (address) {
        var API_KEY = 'b610d05a792943d3aa4ca4b95076f48e';
        var url = "https://api.opencagedata.com/geocode/v1/json?q=" + address.street + "%2C%20" + address.city + "&key=" + API_KEY + "&language=ro&pretty=1";
        return this._http.get(url);
    };
    FarmsrvService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_1__["Http"]])
    ], FarmsrvService);
    return FarmsrvService;
}());



/***/ }),

/***/ "./src/app/farms/geoloc.service.ts":
/*!*****************************************!*\
  !*** ./src/app/farms/geoloc.service.ts ***!
  \*****************************************/
/*! exports provided: GeolocService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GeolocService", function() { return GeolocService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var GeolocService = /** @class */ (function () {
    function GeolocService(_http) {
        this._http = _http;
    }
    GeolocService.prototype.getLatLongFromAddress = function (address) {
        var API_KEY = 'b610d05a792943d3aa4ca4b95076f48e';
        // var address = {
        //     city: "Bucharest",
        //     street: "Zece Mese",
        //     number: "8"
        // };
        // https://api.opencagedata.com/geocode/v1/json?q=Moabit%2C%20Berlin&key=b610d05a792943d3aa4ca4b95076f48e&language=ro&pretty=1
        var url = "https://api.opencagedata.com/geocode/v1/json?q=" + address.street + "%2C%20" + address.city + "&key=" + API_KEY + "&language=ro&pretty=1";
        console.log('making request to URL: ' + url);
        return this._http.get(url);
    };
    GeolocService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_1__["Http"]])
    ], GeolocService);
    return GeolocService;
}());



/***/ }),

/***/ "./src/app/farms/irigation.service.ts":
/*!********************************************!*\
  !*** ./src/app/farms/irigation.service.ts ***!
  \********************************************/
/*! exports provided: IrigationService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IrigationService", function() { return IrigationService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var IrigationService = /** @class */ (function () {
    function IrigationService(httpClient) {
        this.httpClient = httpClient;
        this.restIrigationUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].contextPathServer + '/rest/irigation';
    }
    IrigationService.prototype.appendToken = function () {
        return "?access_token=" + localStorage.getItem('access_token');
    };
    // http://localhost:9333/rest/irigation/bydate/2019/5/10
    // http://localhost:9333/rest/irigation/byid/46
    IrigationService.prototype.findOperationPropertyById = function (id) {
        return this.httpClient.get(this.restIrigationUrl + '/byid/' + id + this.appendToken());
    };
    IrigationService.prototype.findSectorProperties = function (year, month, day) {
        return this.httpClient.get(this.restIrigationUrl + "/bydate/" + year + "/" + month + "/" + day + this.appendToken());
    };
    IrigationService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_http__WEBPACK_IMPORTED_MODULE_1__["Http"]])
    ], IrigationService);
    return IrigationService;
}());



/***/ }),

/***/ "./src/app/farms/irigation/irigation.component.css":
/*!*********************************************************!*\
  !*** ./src/app/farms/irigation/irigation.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/farms/irigation/irigation.component.html":
/*!**********************************************************!*\
  !*** ./src/app/farms/irigation/irigation.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\n  <div *ngIf=\"irigationOperation\">\n\n\n    <div id=\"map\" style=\"width: 400px; height: 400px;\">\n\n    </div>\n\n    Location name: {{irigationOperation.dtoWeather.name}}\n    <hr> Soil type: {{irigationOperation.soilType.name}}\n    \n    <h2> Weather (at the time the operation was applied) </h2>\n    <br> Temperature: {{irigationOperation.dtoWeather.temp}} &deg; C\n    <br> Pressure: {{irigationOperation.dtoWeather.pressure}} Bar\n    <br> Humidity (atmospheric): {{irigationOperation.dtoWeather.humidity}} %\n    <br> Windspeed: {{irigationOperation.dtoWeather.windSpeed}} m/s\n    <br>\n    <hr> Prop name name: {{irigationOperation.farmOperationProperty.propertyName}}\n    <br> Prop value (quantity): {{irigationOperation.farmOperationProperty.propertyValue}}\n    <br>\n\n    <!-- irigare -->\n    Opearation name: {{irigationOperation.farmOperationProperty.farmOperation.operationName}}\n    <br> Date applied: {{irigationOperation.farmOperationProperty.dateApplied}}\n    <br>\n\n\n  </div>\n  <hr>\n  <div>\n    <div class=\"form-group\">\n      <label for=\"inputSectorPropertyType\">Select operation</label>\n      <select (change)=\"onChange($event.target)\" [(ngModel)]=\"sectorPropertyTypeSelected\" style=\"width: 50%;\" class=\"form-control\"\n        id=\"inputSectorPropertyType\">\n        <option *ngFor=\"let sp of sectorPropertyTypes\" [ngValue]=\"sp\">{{sp}}</option>\n      </select>\n    </div>\n  </div>\n  <div>\n    Properties:\n    <div *ngFor=\"let sprop of sectorProperties\" style=\"margin-bottom: 30px;\">\n\n      Measurement date: {{sprop.dateMeasured | date : 'yyyy-MM-dd h:mm a'}}\n      <br> id: {{sprop.id}}\n      <br> Property name: {{sprop.propName}}\n      <br> Property value: {{sprop.propValue}}\n      <br>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/farms/irigation/irigation.component.ts":
/*!********************************************************!*\
  !*** ./src/app/farms/irigation/irigation.component.ts ***!
  \********************************************************/
/*! exports provided: IrigationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IrigationComponent", function() { return IrigationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _irigation_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../irigation.service */ "./src/app/farms/irigation.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _map_loader__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../map.loader */ "./src/app/farms/map.loader.ts");
/* harmony import */ var _logout_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../logout.service */ "./src/app/logout.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var IrigationComponent = /** @class */ (function () {
    function IrigationComponent(irigationService, activatedRoute, logoutService) {
        this.irigationService = irigationService;
        this.activatedRoute = activatedRoute;
        this.logoutService = logoutService;
        this.sectorProperties = [];
        this.sectorPropertyTypes = []; // umiditate, temperatura etc.
        this.sectorPropertyTypeSelected = '';
        this.initialSectorProperties = [];
    }
    IrigationComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.activatedRoute.params.subscribe(function (params) {
            _this.irigationOperationId = params['id'];
            _this.irigationService.findOperationPropertyById(_this.irigationOperationId).subscribe(function (rez) {
                _this.irigationOperation = rez.json();
                console.log('Irigation operation loaded: ', _this.irigationOperation);
                var dateApplied = _this.irigationOperation.farmOperationProperty.dateApplied;
                console.log('date Applied = = = ', dateApplied);
                var dateElements = ('' + dateApplied).split('-');
                var year = parseInt(dateElements[0]);
                var month = parseInt(dateElements[1]);
                var day = parseInt(dateElements[2]);
                _this.irigationService.findSectorProperties(year, month, day).subscribe(function (rez) {
                    _this.sectorProperties = rez.json();
                    _this.initialSectorProperties = rez.json().slice();
                    var uniquePropertyTypes = new Set();
                    console.log('sector properties for date: ', _this.sectorProperties);
                    for (var _i = 0, _a = _this.sectorProperties; _i < _a.length; _i++) {
                        var sp = _a[_i];
                        uniquePropertyTypes.add(sp.propName);
                    }
                    _this.sectorPropertyTypes = Array.from(uniquePropertyTypes);
                    console.log('SECTOR PROP TYPES = ', _this.sectorPropertyTypes);
                }, function (err) {
                    console.log('could not load sector properties, err = ', err);
                });
            }, function (err) {
                console.log('could not load operation property', err);
                console.log('ERROR STATUS: ', err.status);
                if (err.status == 401) {
                    _this.logoutService.openDialog();
                }
            });
        });
    };
    IrigationComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        console.log('after view init sector map component');
        _map_loader__WEBPACK_IMPORTED_MODULE_3__["MapLoaderService"].load().then(function () {
            _this.drawPolygon();
        });
    };
    IrigationComponent.prototype.onChange = function (sectorPropertyType) {
        var spname = sectorPropertyType.value.split(":")[1].trim();
        var filteredSectorProperties = this.initialSectorProperties.filter(function (x) { return x.propName == spname; });
        this.sectorProperties = filteredSectorProperties;
    };
    IrigationComponent.prototype.drawCoordinates = function (coordinatesObject, map) {
        console.log('drawing: ', coordinatesObject);
        var sectorPolygon = new google.maps.Polygon({
            paths: coordinatesObject,
            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#FF0000',
            fillOpacity: 0.35,
            editable: true
        });
        sectorPolygon.setMap(map);
    };
    IrigationComponent.prototype.drawPolygon = function () {
        // TODO: center the map to the specific area in which the farm sector is located
        this.map = new google.maps.Map(document.getElementById('map'), {
            center: { lat: -34.397, lng: 150.644 },
            zoom: 8
        });
        this.drawingManager = new google.maps.drawing.DrawingManager({
            drawingMode: google.maps.drawing.OverlayType.POLYGON,
            drawingControl: true,
            drawingControlOptions: {
                position: google.maps.ControlPosition.TOP_CENTER,
                drawingModes: ['polygon']
            }
        });
        this.drawingManager.setMap(this.map);
        this.drawCoordinates(this.irigationOperation.dtoPolygon.coordinates, this.map);
    };
    IrigationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-irigation',
            template: __webpack_require__(/*! ./irigation.component.html */ "./src/app/farms/irigation/irigation.component.html"),
            styles: [__webpack_require__(/*! ./irigation.component.css */ "./src/app/farms/irigation/irigation.component.css")]
        }),
        __metadata("design:paramtypes", [_irigation_service__WEBPACK_IMPORTED_MODULE_1__["IrigationService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _logout_service__WEBPACK_IMPORTED_MODULE_4__["LogoutService"]])
    ], IrigationComponent);
    return IrigationComponent;
}());



/***/ }),

/***/ "./src/app/farms/map.loader.ts":
/*!*************************************!*\
  !*** ./src/app/farms/map.loader.ts ***!
  \*************************************/
/*! exports provided: MapLoaderService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapLoaderService", function() { return MapLoaderService; });
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");

var MapLoaderService = /** @class */ (function () {
    function MapLoaderService() {
    }
    MapLoaderService.load = function () {
        var _this = this;
        var browserKey = _environments_environment__WEBPACK_IMPORTED_MODULE_0__["environment"].API_KEY_GOOOGLE;
        var map = {
            URL: 'https://maps.googleapis.com/maps/api/js?libraries=geometry,drawing&key=' + browserKey + '&callback=__onGoogleLoaded',
        };
        // First time 'load' is called?
        if (!this.promise) {
            // Make promise to load
            this.promise = new Promise(function (resolve) {
                _this.loadScript(map.URL);
                // Set callback for when google maps is loaded.
                window['__onGoogleLoaded'] = function ($event) {
                    resolve('google maps api loaded');
                };
            });
        }
        // Always return promise. When 'load' is called many times, the promise is already resolved.
        return this.promise;
    };
    //this function will work cross-browser for loading scripts asynchronously
    MapLoaderService.loadScript = function (src, callback) {
        var s, r, t;
        r = false;
        s = document.createElement('script');
        s.type = 'text/javascript';
        s.src = src;
        s.onload = s.onreadystatechange = function () {
            //console.log( this.readyState ); //uncomment this line to see which ready states are called.
            if (!r && (!this.readyState || this.readyState == 'complete')) {
                r = true;
                if (typeof callback === "function")
                    callback();
            }
        };
        t = document.getElementsByTagName('script')[0];
        t.parentNode.insertBefore(s, t);
    };
    return MapLoaderService;
}());



/***/ }),

/***/ "./src/app/farms/reports-sd-properties/reports-sd-properties.component.css":
/*!*********************************************************************************!*\
  !*** ./src/app/farms/reports-sd-properties/reports-sd-properties.component.css ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/farms/reports-sd-properties/reports-sd-properties.component.html":
/*!**********************************************************************************!*\
  !*** ./src/app/farms/reports-sd-properties/reports-sd-properties.component.html ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\n\n\n\n  Sector: {{idSector}} .\n  <br>\n\n\n  <div *ngIf=\"reportError\" class=\"alert alert-danger\" role=\"alert\">\n      PLEASE SELECT REPORT TYPE FROM THE DROPDOWN\n    </div>\n\n    \n  <select [(ngModel)]=\"selectedReportType\">\n\n      <option *ngFor=\"let pt of propertyTypes\" [ngValue]=\"pt\">{{pt}}</option>\n    \n    </select>\n    Generating report for: {{selectedReportType}}\n  <button (click)=\"generateReport()\">Generate report</button>\n\n  <div *ngIf=\"isReportDrawn\">\n    <google-chart style=\"width: 100%;\" [data]=\"chartData\"  [type]=\"'LineChart'\" [columnNames]=\"chartColumnNames\"></google-chart>\n  </div>\n  \n  \n\n\n</div>\n\n"

/***/ }),

/***/ "./src/app/farms/reports-sd-properties/reports-sd-properties.component.ts":
/*!********************************************************************************!*\
  !*** ./src/app/farms/reports-sd-properties/reports-sd-properties.component.ts ***!
  \********************************************************************************/
/*! exports provided: ReportsSdPropertiesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReportsSdPropertiesComponent", function() { return ReportsSdPropertiesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _farmsrv_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../farmsrv.service */ "./src/app/farms/farmsrv.service.ts");
/* harmony import */ var _logout_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../logout.service */ "./src/app/logout.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ReportsSdPropertiesComponent = /** @class */ (function () {
    function ReportsSdPropertiesComponent(activatedRouterService, service, logoutService) {
        this.activatedRouterService = activatedRouterService;
        this.service = service;
        this.logoutService = logoutService;
        this.refreshSectorPropertyInterval = null;
        this.propertyTypes = [];
        this.selectedReportType = '';
        this.reportError = false;
        this.chartData = [];
        this.chartColumnNames = [];
        this.isReportDrawn = false;
    }
    ReportsSdPropertiesComponent.prototype.ngOnDestroy = function () {
        console.log('stopping request interval');
        clearInterval(this.refreshSectorPropertyInterval);
    };
    ReportsSdPropertiesComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.activatedRouterService.params.subscribe(function (params) {
            _this.idSector = params['idsector'];
            _this.service.findSectorById(_this.idSector).subscribe(function (rez) {
                _this.sector = rez.json(); // load sector
                _this.initReport();
                _this.startInterval();
            }, function (err) {
                console.log('error loading farm sector ');
                console.log('ERROR STATUS: ', err.status);
                if (err.status == 401) {
                    _this.logoutService.openDialog();
                }
            });
        });
    };
    ReportsSdPropertiesComponent.prototype.startInterval = function () {
        var _this = this;
        this.refreshSectorPropertyInterval = setInterval(function () {
            _this.refreshAllSectorProperties();
            _this.generateReport(); // refrehs report
        }, 3000);
    };
    ReportsSdPropertiesComponent.prototype.initReport = function () {
        var propertyNames = new Set();
        var propertyNamesArray = [];
        for (var _i = 0, _a = this.sector.farmSectorProperties; _i < _a.length; _i++) {
            var prop = _a[_i];
            propertyNames.add(prop.propName);
        }
        propertyNames.forEach(function (x) { propertyNamesArray.push(x); });
        for (var _b = 0, propertyNamesArray_1 = propertyNamesArray; _b < propertyNamesArray_1.length; _b++) {
            var pn = propertyNamesArray_1[_b];
            this.propertyTypes.push(pn);
        }
        console.log('Property types array = ', this.propertyTypes);
    };
    ReportsSdPropertiesComponent.prototype.generateReport = function () {
        if (this.selectedReportType == '') {
            this.reportError = true;
            return;
        }
        this.reportError = false;
        var propertyNames = new Set();
        var propertyNamesArray = [];
        for (var _i = 0, _a = this.sector.farmSectorProperties; _i < _a.length; _i++) {
            var prop = _a[_i];
            propertyNames.add(prop.propName);
        }
        propertyNames.forEach(function (x) { propertyNamesArray.push(x); });
        var groupedProperties = {};
        for (var _b = 0, propertyNamesArray_2 = propertyNamesArray; _b < propertyNamesArray_2.length; _b++) {
            var pn = propertyNamesArray_2[_b];
            groupedProperties[pn] = [];
        }
        console.log('SECTOR PROPERTIES FROM SERVER: ', this.sector.farmSectorProperties);
        this.sector.farmSectorProperties.map(function (x) {
            groupedProperties[x.propName].push(x);
        });
        console.log("Grouped properties: ", groupedProperties);
        this.chartData = this.generateChart(groupedProperties, this.selectedReportType);
        this.isReportDrawn = true;
        console.log('date umiditate test: ', this.chartData);
    };
    ReportsSdPropertiesComponent.prototype.generateChart = function (groupedProperties, propertyName) {
        var chartData = [];
        var DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
        for (var _i = 0, _a = groupedProperties[propertyName]; _i < _a.length; _i++) {
            var entry = _a[_i];
            var dateOk = new Date(entry.dateMeasured);
            console.log('DATE OKAY: ', dateOk.getTime());
            chartData.push([dateOk.getTime(), parseInt(entry.propValue)]);
        }
        this.chartColumnNames = ['date measured', propertyName];
        function Comparator(a, b) {
            return a[0] - b[0];
        }
        chartData = chartData.sort(Comparator);
        chartData = chartData.map(function (x) {
            console.log('element x = ', x);
            var dateElement = new Date(x[0]);
            x[0] = dateElement.getFullYear() + "-" + dateElement.getMonth() + "-" + dateElement.getUTCDate();
            return x;
        });
        console.log('chart Data - ', chartData);
        return chartData;
    };
    ReportsSdPropertiesComponent.prototype.refreshAllSectorProperties = function () {
        var _this = this;
        this.service.findAllSectorProperties(this.sector.id).subscribe(function (rez) {
            _this.sector.farmSectorProperties = rez.json();
            for (var _i = 0, _a = _this.sector.farmSectorProperties; _i < _a.length; _i++) {
                var fsp = _a[_i];
                fsp.dateMeasured = _this.formatDate(fsp.dateMeasured);
            }
            console.log('SECTOR PROPERTIES FOR REPORT: ', _this.sector.farmSectorProperties);
        }, function (err) {
            console.log('could not load properties for current sector: ', err);
        });
    };
    ReportsSdPropertiesComponent.prototype.formatDate = function (date) {
        var d = new Date(date), month = '' + (d.getMonth() + 1), day = '' + d.getDate(), year = d.getFullYear();
        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;
        return [year, month, day].join('-');
    };
    ReportsSdPropertiesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-reports-sd-properties',
            template: __webpack_require__(/*! ./reports-sd-properties.component.html */ "./src/app/farms/reports-sd-properties/reports-sd-properties.component.html"),
            styles: [__webpack_require__(/*! ./reports-sd-properties.component.css */ "./src/app/farms/reports-sd-properties/reports-sd-properties.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _farmsrv_service__WEBPACK_IMPORTED_MODULE_2__["FarmsrvService"], _logout_service__WEBPACK_IMPORTED_MODULE_3__["LogoutService"]])
    ], ReportsSdPropertiesComponent);
    return ReportsSdPropertiesComponent;
}());



/***/ }),

/***/ "./src/app/farms/sector-map/sector-map.component.css":
/*!***********************************************************!*\
  !*** ./src/app/farms/sector-map/sector-map.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#map {\r\n    height: 500px;\r\n}"

/***/ }),

/***/ "./src/app/farms/sector-map/sector-map.component.html":
/*!************************************************************!*\
  !*** ./src/app/farms/sector-map/sector-map.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\n  Is Shape Drawn: <span *ngIf=\"isShapeDrawn; else noshape\">TRUE</span>\n  <ng-template #noshape>\n    <span>FALSE</span>\n  </ng-template>\n  <button *ngIf=\"isShapeDrawn\" (click)=\"removeShapes()\">Remove shapes</button>\n  <hr>\n  <!-- ------------------- sector map ------------------- ---    -->\n  <div id=\"map\" style=\"width: 400px; height: 400px;\">\n\n  </div>\n  <div *ngIf=\"farmSector && farmSector.dtoWeather\">\n\n    \n     Humidity: {{farmSector.dtoWeather.humidity}} <br>\n     Local name: {{farmSector.dtoWeather.name}} <br>\n     Pressure: {{farmSector.dtoWeather.pressure}} <br>\n     Temperature: {{farmSector.dtoWeather.temp}} <br>\n     Wind Speed:: {{farmSector.dtoWeather.windSpeed}} <br>\n  </div>\n\n  \n</div>"

/***/ }),

/***/ "./src/app/farms/sector-map/sector-map.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/farms/sector-map/sector-map.component.ts ***!
  \**********************************************************/
/*! exports provided: SectorMapComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SectorMapComponent", function() { return SectorMapComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _map_loader__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../map.loader */ "./src/app/farms/map.loader.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _farmsrv_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../farmsrv.service */ "./src/app/farms/farmsrv.service.ts");
/* harmony import */ var _logout_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../logout.service */ "./src/app/logout.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var SectorMapComponent = /** @class */ (function () {
    function SectorMapComponent(route, service, logoutService) {
        this.route = route;
        this.service = service;
        this.logoutService = logoutService;
        this.selectedShape = null;
        this.allShapes = [];
        this.farmSector = null;
        this.isShapeDrawn = false;
        // bounds of the desired area
        this.allowedBounds = null;
        this.lastValidCenter = null;
    }
    SectorMapComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.sectorId = params['idsector'];
            _this.service.findSectorById(_this.sectorId).subscribe(function (rez) {
                console.log('FOUND farm sector: ', rez.json());
                _this.farmSector = rez.json();
                _this.drawPolygon();
            }, function (err) {
                console.log('error loading farm sector');
                console.log('ERROR STATUS: ', err.status);
                if (err.status == 401) {
                    _this.logoutService.openDialog();
                }
            });
        });
    };
    SectorMapComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        console.log('after view init sector map component');
        _map_loader__WEBPACK_IMPORTED_MODULE_1__["MapLoaderService"].load().then(function () {
            _this.drawPolygon();
            _this.allowedBounds = new google.maps.LatLngBounds(new google.maps.LatLng(43.623308113, 20.2331701528), new google.maps.LatLng(48.2202406655, 29.8352209341));
        });
    };
    SectorMapComponent.prototype.drawCoordinates = function (coordinatesObject, map) {
        console.log('drawing: ', coordinatesObject);
        var sectorPolygon = new google.maps.Polygon({
            paths: coordinatesObject,
            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#FF0000',
            fillOpacity: 0.35,
            editable: true
        });
        sectorPolygon.setMap(map);
        google.maps.event.addListener(sectorPolygon, 'click', function () {
            console.log('click on drawn polygon');
        });
        this.attachEventListeners(sectorPolygon);
    };
    SectorMapComponent.prototype.drawPolygon = function () {
        var _this = this;
        this.map = new google.maps.Map(document.getElementById('map'), {
            center: { lat: 44.439663, lng: 26.096306 },
            zoom: 8
        });
        this.lastValidCenter = this.map.getCenter();
        console.log('Last valid center initial: ', this.lastValidCenter);
        this.drawingManager = new google.maps.drawing.DrawingManager({
            drawingMode: google.maps.drawing.OverlayType.POLYGON,
            drawingControl: true,
            drawingControlOptions: {
                position: google.maps.ControlPosition.TOP_CENTER,
                drawingModes: ['polygon']
            }
        });
        this.drawingManager.setMap(this.map);
        google.maps.event.addListener(this.map, 'center_changed', function () {
            console.log('Are you on map? ', _this.allowedBounds.contains(_this.map.getCenter()));
            console.log(_this.allowedBounds);
            console.log(_this.map.getCenter());
            if (_this.allowedBounds.contains(_this.map.getCenter())) {
                _this.lastValidCenter = _this.map.getCenter();
            }
            else {
                _this.map.panTo(_this.lastValidCenter);
            }
        });
        google.maps.event.addListener(this.drawingManager, 'polygoncomplete', function (event) {
            if (_this.isShapeDrawn) {
                alert('farm sector map already assigned');
                event.setMap(null);
                return;
            }
            console.log('POLYGON COMPLETE');
            console.log(event);
            _this.shapeToStringDB(event);
            _this.isShapeDrawn = true;
        });
        this.attachEventListeners();
        if (this.farmSector) {
            console.log('DRAWING FARM SECTOR POLYGON - SUCCESS');
            if (this.farmSector.dtoPolygon) {
                console.log('COORDINATES TO DRAW AT: ', this.farmSector.dtoPolygon.coordinates);
                this.drawCoordinates(this.farmSector.dtoPolygon.coordinates, this.map);
                this.isShapeDrawn = true;
            }
            else {
                console.log('NO POLYGON DEFINED FOR CURRENT SECTOR');
            }
        }
        else {
            console.log('DRAWING FARM SECTOR POLYGON - ERROR');
        }
        this.attachEventListeners();
    };
    SectorMapComponent.prototype.attachEventListenersHelper = function (shape) {
        var _this = this;
        google.maps.event.addListener(shape.getPath(), 'insert_at', function () {
            // console.log('**INSERT EVENT*** ADDING SHAPE (new shape inserted), before: ', this.allShapes);
            _this.allShapes.push(shape);
            _this.shapeToStringDB(shape);
        });
        google.maps.event.addListener(shape.getPath(), 'set_at', function () {
            console.log('shape coords updated', _this.getShapeCoords(shape));
            console.log('string of json: ' + JSON.stringify(_this.getShapeCoords(shape)));
            var coordinatesForServer = [];
            for (var _i = 0, _a = _this.getShapeCoords(shape); _i < _a.length; _i++) {
                var coord = _a[_i];
                coordinatesForServer.push({
                    lat: coord.latitude,
                    lng: coord.longitude
                });
            }
            console.log('pushing to server: ', coordinatesForServer);
            _this.service.updateSectorCoordinates(_this.sectorId, coordinatesForServer).subscribe(function (rez) {
                console.log('success update coordinates: ', rez.json());
            }, function (err) {
                console.log('error update coordinates: ', err.json());
            });
            // alert('updated shape, go update it in your db 2');
            _this.shapeToStringDB(shape);
        });
    };
    SectorMapComponent.prototype.attachEventListeners = function (customPolygon) {
        var _this = this;
        if (customPolygon === void 0) { customPolygon = null; }
        if (customPolygon != null) {
            this.attachEventListenersHelper(customPolygon);
            this.allShapes.push(customPolygon);
            console.log('ALL SHAPES:', this.allShapes);
            return;
        }
        google.maps.event.addListener(this.drawingManager, 'overlaycomplete', function (event) {
            var newShape = event.overlay;
            google.maps.event.addListener(newShape, 'click', function () {
                _this.setSelection(newShape);
                console.log('**click on shape');
            });
            // Polygon drawn
            if (event.type === google.maps.drawing.OverlayType.POLYGON) {
                //this is the coordinate, you can assign it to a variable or pass into another function.
                // alert(event.overlay.getPath().getArray());
                _this.allShapes.push(event.overlay);
                console.log('ALL SHAPES:', _this.allShapes);
            }
        });
    };
    /* --- remove shape, select shape --- */
    /**
     * Method which converts the polygon into DTO and sends it
     * via service method to the server endpoint
     * /sectors/update/{idsector}
     * @param shape
     */
    SectorMapComponent.prototype.shapeToStringDB = function (shape) {
        console.log('ADDED TO SHAPES: ', this.allShapes);
        var vertices = shape.getPath();
        var dtoPolygon = { coordinates: [] };
        // console.log('updating:::', coordonate.getArray());
        for (var i = 0; i < vertices.getLength(); i++) {
            var xy = vertices.getAt(i);
            console.log('point: ', xy);
            dtoPolygon.coordinates.push({
                lat: xy.lat(),
                lng: xy.lng()
            });
        }
        this.farmSector.dtoPolygon = dtoPolygon;
        this.service.updateSectorCoordinatesPolygon(this.farmSector.id, this.farmSector).subscribe(function (rez) {
            console.log('result: ', rez.json());
        }, function (err) {
            console.log('ERR UPDATE: ', err);
        });
        console.log('should update dtoPolygon for: ', dtoPolygon);
    };
    SectorMapComponent.prototype.getShapeCoords = function (shape) {
        var path = shape.getPath();
        var coords = [];
        for (var i = 0; i < path.length; i++) {
            coords.push({
                latitude: path.getAt(i).lat(),
                longitude: path.getAt(i).lng()
            });
        }
        return coords;
    };
    SectorMapComponent.prototype.removeShapes = function () {
        var _this = this;
        console.log('**REMOVING SHAPES**');
        for (var _i = 0, _a = this.allShapes; _i < _a.length; _i++) {
            var sh = _a[_i];
            sh.setMap(null);
        }
        this.service.updateSectorDeleteCoordinates(this.sectorId).subscribe(function (rez) {
            console.log('updated sector: ', rez.json());
            _this.isShapeDrawn = false;
        }, function (err) {
            console.log('eroare: ', err);
        });
    };
    SectorMapComponent.prototype.clearSelection = function () {
        if (this.selectedShape) {
            this.selectedShape.setEditable(false);
            this.selectedShape = null;
        }
    };
    SectorMapComponent.prototype.setSelection = function (shape) {
        this.clearSelection();
        this.selectedShape = shape;
        shape.setEditable(true);
    };
    SectorMapComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-sector-map',
            template: __webpack_require__(/*! ./sector-map.component.html */ "./src/app/farms/sector-map/sector-map.component.html"),
            styles: [__webpack_require__(/*! ./sector-map.component.css */ "./src/app/farms/sector-map/sector-map.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _farmsrv_service__WEBPACK_IMPORTED_MODULE_3__["FarmsrvService"],
            _logout_service__WEBPACK_IMPORTED_MODULE_4__["LogoutService"]])
    ], SectorMapComponent);
    return SectorMapComponent;
}());



/***/ }),

/***/ "./src/app/farms/sector/sector.component.css":
/*!***************************************************!*\
  !*** ./src/app/farms/sector/sector.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/farms/sector/sector.component.html":
/*!****************************************************!*\
  !*** ./src/app/farms/sector/sector.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\n\n  <div *ngIf=\"sector\">\n    <div>Description: {{sector.sectorDescription}}</div>\n    <div>\n      <a [routerLink]=\"['/reports/spropertis', sector.id]\">GENERATE REPORT</a>\n    </div>\n    \n    <!-- -------------------- sector properties -------------------- -->\n\n\n    \n    <button *ngIf=\"!showSectorDetails\" (click)=\"showSectorDetails == true ? emptyAction() : (showProps = !showProps)\">Show properties (current)</button>\n    <button *ngIf=\"!showProps\" (click)=\"showProps == true ? emptyAction() :  (showSectorDetails = !showSectorDetails)\">Show sector details</button>\n\n\n    <div *ngIf=\"showProps && !showSectorDetails\">\n      <!-- <button (click)=\"showReports()\">Show reports</button> -->\n      <a class=\"btn btn-info\" [routerLink]=\"['/reports/spropertis/', sector.id]\">Show SProperties report</a>\n      <ul style=\"color: green;\">\n        <li *ngFor=\"let p of sector.farmSectorProperties\">\n          <input type=\"hidden\" [(ngModel)]=\"p.id\">\n\n          <div class=\"form-group\">\n            <label for=\"inputPropertyNameSector\">Property name</label>\n            <input style=\"width: 50%;\" [(ngModel)]=\"p.propName\" class=\"form-control\" id=\"inputPropertyNameSector\" placeholder=\"Property Name\">\n          </div>\n\n\n\n          <div class=\"form-group\">\n            <label for=\"inputPropertyValueSector\">Property value</label>\n            <input style=\"width: 50%;\" [(ngModel)]=\"p.propValue\" class=\"form-control\" id=\"inputPropertyValueSector\" placeholder=\"Property Value\">\n          </div>\n\n          <div class=\"form-group\">\n              <label for=\"inputPropertyValueSector\">Date measured</label>\n              <input style=\"width: 50%;\" [(ngModel)]=\"p.dateMeasured \"  class=\"form-control\" id=\"inputPropertyValueSector\" placeholder=\"Property Value\" readonly>\n            </div>\n\n          <br>\n          <button (click)=\"updateProperty(sector.id, p)\">Update</button>\n          <br>\n\n          <button (click)=\"deleteSectorProperty(p)\">Del sp</button>\n        </li>\n      </ul>\n      <h3>Add sector property</h3>\n\n\n\n      <div class=\"form-group\">\n          <label for=\"inputPropertyNameSectorAdd\">Property name</label>\n          <input style=\"width: 50%;\" [(ngModel)]=\"sector.sectorPropertyName\" class=\"form-control\" id=\"inputPropertyNameSectorAdd\" placeholder=\"Property Name\">\n        </div>\n\n\n\n        <div class=\"form-group\">\n          <label for=\"inputPropertyValueSectorAdd\">Property value</label>\n          <input style=\"width: 50%;\" [(ngModel)]=\"sector.sectorPropertyValue\" class=\"form-control\" id=\"inputPropertyValueSectorAdd\" placeholder=\"Property Value\">\n        </div>\n\n\n\n\n<!-- \n      Property Name\n      <input [(ngModel)]=\"sector.sectorPropertyName\">\n      <br> TEST: {{sector.sectorPropertyName}}\n      <br> Property Value\n      <input [(ngModel)]=\"sector.sectorPropertyValue\">\n      <br> -->\n      <button class=\"btn btn-info\" (click)=\"saveSectorProperty(sector.id, sector.sectorPropertyName, sector.sectorPropertyValue)\">Save property</button>\n\n    </div>\n\n\n\n    <div *ngIf=\"showSectorDetails && !showProps\">\n      <app-farms-sector-details [sector]=\"sector\" (evenimentSectorDetailsComponent)=\"evenimentSector()\"></app-farms-sector-details>\n    </div>\n  </div>\n\n</div>"

/***/ }),

/***/ "./src/app/farms/sector/sector.component.ts":
/*!**************************************************!*\
  !*** ./src/app/farms/sector/sector.component.ts ***!
  \**************************************************/
/*! exports provided: SectorComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SectorComponent", function() { return SectorComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _farmsrv_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../farmsrv.service */ "./src/app/farms/farmsrv.service.ts");
/* harmony import */ var _geoloc_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../geoloc.service */ "./src/app/farms/geoloc.service.ts");
/* harmony import */ var _logout_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../logout.service */ "./src/app/logout.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var SectorComponent = /** @class */ (function () {
    function SectorComponent(route, service, geoloc, logoutService) {
        this.route = route;
        this.service = service;
        this.geoloc = geoloc;
        this.logoutService = logoutService;
        this.sector = null; // farm sector contains farm sector details (associated with operations)
        this.refreshSectorPropertyInterval = null;
        this.showSectorDetails = false;
    }
    SectorComponent.prototype.ngOnDestroy = function () {
        console.log('sector.component.ts - ngOnDestroy');
        console.log('destroying sector component - no need execute interval');
        this.stopInterval();
    };
    SectorComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log('sector.component.ts - ngOnInit');
        this.route.params.subscribe(function (params) {
            _this.idSector = params['idsector'];
            _this.service.findSectorById(_this.idSector).subscribe(function (rez) {
                _this.sector = rez.json(); // load sector
                _this.startInterval();
            }, function (err) {
                console.log('error loading farm sector ');
                console.log('ERROR STATUS: ', err.status);
                if (err.status == 401) {
                    // which will result in the user being redirected to the
                    // login component (via router)
                    _this.logoutService.openDialog();
                }
            });
        });
    };
    SectorComponent.prototype.formatDate = function (date) {
        var d = new Date(date), month = '' + (d.getMonth() + 1), day = '' + d.getDate(), year = d.getFullYear();
        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;
        return [year, month, day].join('-');
    };
    SectorComponent.prototype.startInterval = function () {
        var _this = this;
        this.refreshSectorPropertyInterval = setInterval(function () {
            _this.refreshAllSectorProperties();
        }, 3000);
    };
    SectorComponent.prototype.stopInterval = function () {
        console.log('stopping interval - ');
        clearInterval(this.refreshSectorPropertyInterval);
    };
    SectorComponent.prototype.refreshAllSectorProperties = function () {
        var _this = this;
        this.service.findAllSectorProperties(this.sector.id).subscribe(function (rez) {
            _this.sector.farmSectorProperties = rez.json();
            for (var _i = 0, _a = _this.sector.farmSectorProperties; _i < _a.length; _i++) {
                var fsp = _a[_i];
                fsp.dateMeasured = _this.formatDate(fsp.dateMeasured);
            }
        }, function (err) {
            console.log('could not load properties for current sector: ', err);
        });
    };
    SectorComponent.prototype.evenimentSector = function () {
        var _this = this;
        console.log('SHOULD SEND REQUEST FOR FARM SECTOR AGAIN');
        this.service.findSectorById(this.idSector).subscribe(function (rez) {
            _this.sector = rez.json();
        }, function (err) {
            console.log('error loading farm sector ');
        });
    };
    SectorComponent.prototype.deleteSectorProperty = function (prop) {
        var _this = this;
        this.service.deleteSectorProperty(prop.id).subscribe(function (rez) {
            console.log('saved new sector', rez.json());
            _this.sector = rez.json();
        }, function (err) {
            console.log('error: ', err);
        });
    };
    SectorComponent.prototype.emptyAction = function () {
        console.log('nothing');
    };
    SectorComponent.prototype.updateProperty = function (sectorid, p) {
        console.log('updating: ' + sectorid + " property: ", p);
        this.service.updateProperty(sectorid, p).subscribe(function (rez) {
            console.log('okay', rez.json());
        }, function (err) {
            console.log('error:', err);
            alert('Could not update sector property');
        });
    };
    SectorComponent.prototype.saveSectorProperty = function (sectorId, sectorPropertyName, sectorPropertyValue) {
        var _this = this;
        console.log('saving sector property: ' + sectorId + " PNAME " + sectorPropertyName + " PVALUE " + sectorPropertyValue);
        var dtoBody = {
            farmSectorId: sectorId,
            propertyName: sectorPropertyName,
            propertyValue: sectorPropertyValue,
        };
        this.service.saveFarmSectorProperty(dtoBody).subscribe(function (rez) {
            console.log('saved dto', rez.json());
            _this.service.findSectorById(_this.idSector).subscribe(function (rez) {
                _this.sector = rez.json();
                console.log('current sector: ', _this.sector);
            });
        }, function (err) {
            console.log(err);
            alert('Could not save Farm Sector Property');
        });
    };
    SectorComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-sector',
            template: __webpack_require__(/*! ./sector.component.html */ "./src/app/farms/sector/sector.component.html"),
            styles: [__webpack_require__(/*! ./sector.component.css */ "./src/app/farms/sector/sector.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"], _farmsrv_service__WEBPACK_IMPORTED_MODULE_2__["FarmsrvService"],
            _geoloc_service__WEBPACK_IMPORTED_MODULE_3__["GeolocService"],
            _logout_service__WEBPACK_IMPORTED_MODULE_4__["LogoutService"]])
    ], SectorComponent);
    return SectorComponent;
}());



/***/ }),

/***/ "./src/app/farms/sectorinterval.service.ts":
/*!*************************************************!*\
  !*** ./src/app/farms/sectorinterval.service.ts ***!
  \*************************************************/
/*! exports provided: SectorintervalService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SectorintervalService", function() { return SectorintervalService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SectorintervalService = /** @class */ (function () {
    function SectorintervalService(httpClient) {
        this.httpClient = httpClient;
        this.restIntervalUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].contextPathServer + '/rest/interval';
    }
    SectorintervalService.prototype.appendToken = function () {
        return "?access_token=" + localStorage.getItem('access_token');
    };
    // dtobysectorid/{sectorid}
    SectorintervalService.prototype.findIntervalsForSector = function (sectorid) {
        return this.httpClient.get(this.restIntervalUrl + "/dtobysectorid/" + sectorid);
    };
    SectorintervalService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], SectorintervalService);
    return SectorintervalService;
}());



/***/ }),

/***/ "./src/app/home/home.component.css":
/*!*****************************************!*\
  !*** ./src/app/home/home.component.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/home/home.component.html":
/*!******************************************!*\
  !*** ./src/app/home/home.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Page Wrapper -->\n<div id=\"wrapper\">\n  <!-- Sidebar -->\n  <ul class=\"navbar-nav bg-gradient-primary sidebar sidebar-dark accordion\" id=\"accordionSidebar\">\n\n    <!-- Sidebar - Brand -->\n    <a class=\"sidebar-brand d-flex align-items-center justify-content-center\" href=\"index.html\">\n\n      <div class=\"sidebar-brand-text mx-3\">Smartfarm</div>\n    </a>\n\n    <!-- Divider -->\n    <hr class=\"sidebar-divider my-0\">\n\n\n\n    <!-- Divider -->\n    <hr class=\"sidebar-divider\">\n\n    <!-- Heading -->\n    <div class=\"sidebar-heading\">\n      Admin\n    </div>\n\n\n\n\n    <!-- Nav Item - Pages Collapse Menu -->\n    <li class=\"nav-item\">\n      <a class=\"nav-link collapsed\" data-toggle=\"collapse\" data-target=\"#collapseTwo\" aria-expanded=\"true\" aria-controls=\"collapseTwo\">\n        <i class=\"fas fa-fw fa-cog\"></i>\n        <span>Farms</span>\n      </a>\n      <div id=\"collapseTwo\" class=\"collapse\" aria-labelledby=\"headingTwo\" data-parent=\"#accordionSidebar\">\n        <div class=\"bg-white py-2 collapse-inner rounded\">\n          <h6 class=\"collapse-header\">Custom Components:</h6>\n          <a class=\"collapse-item\" [routerLink]=\"['/app-farms']\" routerLinkActive=\"active\">App farms</a>\n\n\n        </div>\n      </div>\n    </li>\n\n\n\n    <!-- Divider -->\n    <hr class=\"sidebar-divider\">\n\n    <!-- Heading -->\n    <div class=\"sidebar-heading\">\n      User (farmer)\n    </div>\n\n    <!-- Nav Item - Pages Collapse Menu -->\n    <li class=\"nav-item\">\n      <a class=\"nav-link collapsed\" href=\"#\" data-toggle=\"collapse\" data-target=\"#collapsePages\" aria-expanded=\"true\" aria-controls=\"collapsePages\">\n        <i class=\"fas fa-fw fa-folder\"></i>\n        <span>Pages</span>\n      </a>\n      <div id=\"collapsePages\" class=\"collapse\" aria-labelledby=\"headingPages\" data-parent=\"#accordionSidebar\">\n        <div class=\"bg-white py-2 collapse-inner rounded\">\n          <h6 class=\"collapse-header\">Navigation:</h6>\n          <a class=\"collapse-item\" [routerLink]=\"['about']\">About Smartfarm</a>\n\n          <div class=\"collapse-divider\"></div>\n          \n        </div>\n      </div>\n    </li>\n\n\n\n\n    <!-- Divider -->\n    <hr class=\"sidebar-divider d-none d-md-block\">\n\n\n  </ul>\n  <!-- End of Sidebar -->\n\n  <!-- Content Wrapper -->\n  <div id=\"content-wrapper\" class=\"d-flex flex-column\">\n\n    <!-- Main Content -->\n    <div id=\"content\">\n\n      <!-- Topbar -->\n      <nav class=\"navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow\">\n\n        <!-- Sidebar Toggle (Topbar) -->\n        <button id=\"sidebarToggleTop\" class=\"btn btn-link d-md-none rounded-circle mr-3\">\n          <i class=\"fa fa-bars\"></i>\n        </button>\n\n        <app-search></app-search>\n\n        <!-- Topbar Navbar -->\n        <ul class=\"navbar-nav ml-auto\">\n\n          <!-- Nav Item - Search Dropdown (Visible Only XS) -->\n          <li class=\"nav-item dropdown no-arrow d-sm-none\">\n            <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"searchDropdown\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\"\n              aria-expanded=\"false\">\n              <i class=\"fas fa-search fa-fw\"></i>\n            </a>\n            <!-- Dropdown - Messages -->\n            <div class=\"dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in\" aria-labelledby=\"searchDropdown\">\n              <form class=\"form-inline mr-auto w-100 navbar-search\">\n                <div class=\"input-group\">\n                  <input type=\"text\" class=\"form-control bg-light border-0 small\" placeholder=\"Search for...\" aria-label=\"Search\" aria-describedby=\"basic-addon2\">\n\n                  <div class=\"input-group-append\">\n                    <button class=\"btn btn-primary\" type=\"button\">\n                      <i class=\"fas fa-search fa-sm\"></i>\n                    </button>\n                  </div>\n                </div>\n              </form>\n            </div>\n          </li>\n\n\n\n\n\n          <div class=\"topbar-divider d-none d-sm-block\"></div>\n\n          <!-- Nav Item - User Information -->\n          <li class=\"nav-item dropdown no-arrow\">\n            <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"userDropdown\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\"\n              aria-expanded=\"false\">\n              <span *ngIf=\"securityDto\" class=\"mr-2 d-none d-lg-inline text-gray-600 small\">{{securityDto.username}}</span>\n              <a [routerLink]=\"['login']\">Change User / Login</a>\n            </a>\n\n          </li>\n\n        </ul>\n\n      </nav>\n      <!-- End of Topbar -->\n\n      <!-- Begin Page Content -->\n      <div class=\"container-fluid\">\n\n        <!-- Page Heading -->\n        <div class=\"d-sm-flex align-items-center justify-content-between mb-4\">\n          <h1 class=\"h3 mb-0 text-gray-800\"></h1>\n\n        </div>\n\n\n\n        <!-- Content Row -->\n        <div class=\"row\">\n\n\n\n          <div class=\"col-lg-12 mb-4\">\n\n\n            <!-- CONTENT -->\n            <div class=\"card shadow mb-4\">\n              <div class=\"card-header py-3\">\n                <h6 class=\"m-0 font-weight-bold text-primary\"></h6>\n              </div>\n              <div class=\"card-body\">\n                <p>\n                  <router-outlet></router-outlet>\n                </p>\n\n\n\n              </div>\n            </div>\n\n          </div>\n        </div>\n\n      </div>\n      <!-- /.container-fluid -->\n\n    </div>\n    <!-- End of Main Content -->\n\n\n\n    <!-- Footer -->\n    <footer class=\"sticky-footer bg-white\">\n      <div class=\"container my-auto\">\n        <div class=\"copyright text-center my-auto\">\n          <span>Copyright &copy; Smartfarm 2019</span>\n        </div>\n      </div>\n    </footer>\n    <!-- End of Footer -->\n\n  </div>\n  <!-- End of Content Wrapper -->\n\n</div>\n<!-- End of Page Wrapper -->"

/***/ }),

/***/ "./src/app/home/home.component.ts":
/*!****************************************!*\
  !*** ./src/app/home/home.component.ts ***!
  \****************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _user_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../user.service */ "./src/app/user.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HomeComponent = /** @class */ (function () {
    function HomeComponent(userService) {
        this.userService = userService;
    }
    HomeComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.userService.getPrincipalDto().subscribe(function (rez) {
            _this.securityDto = rez;
            console.log('principal = ', _this.securityDto);
        });
    };
    HomeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.component.html */ "./src/app/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.css */ "./src/app/home/home.component.css")]
        }),
        __metadata("design:paramtypes", [_user_service__WEBPACK_IMPORTED_MODULE_1__["UserService"]])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/login/login.component.css":
/*!*******************************************!*\
  !*** ./src/app/login/login.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/login/login.component.html":
/*!********************************************!*\
  !*** ./src/app/login/login.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1>Login</h1>\n    Username: <input type=\"text\" [(ngModel)]=\"loginData.username\" />\n    Password: <input type=\"password\"  [(ngModel)]=\"loginData.password\"/>\n    <button (click)=\"login()\" type=\"submit\">Login</button>\n\n<h1>Register</h1>\n<span>{{registrationMessage}}</span><br>\nUsername: <input type=\"text\" [(ngModel)]=\"registerData.username\" />\nPassword: <input type=\"password\"  [(ngModel)]=\"registerData.password\"/>\nEmail: <input type=\"text\" [(ngModel)]=\"registerData.email\" />\n<button (click)=\"register()\" >Register</button>"

/***/ }),

/***/ "./src/app/login/login.component.ts":
/*!******************************************!*\
  !*** ./src/app/login/login.component.ts ***!
  \******************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../app.service */ "./src/app/app.service.ts");
/* harmony import */ var _register_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../register.service */ "./src/app/register.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var LoginComponent = /** @class */ (function () {
    function LoginComponent(_service, registerService) {
        this._service = _service;
        this.registerService = registerService;
        this.registrationMessage = '';
        this.loginData = { username: "", password: "" };
        this.registerData = {
            username: "",
            email: "",
            password: ""
        };
    }
    LoginComponent.prototype.login = function () {
        this._service.obtainAccessToken(this.loginData);
    };
    LoginComponent.prototype.register = function () {
        var _this = this;
        this.registerService.register(this.registerData).subscribe(function (rez) {
            console.log('successfully registered: ', rez);
            if (rez.operationSuccessful) {
                _this.registerData.username = '';
                _this.registerData.email = '';
                _this.registerData.password = '';
            }
            _this.registrationMessage = rez.operationMessage;
        }, function (err) {
            console.log('failure in registering: ', err);
        });
    };
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'login-form',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.css */ "./src/app/login/login.component.css")],
            providers: [_app_service__WEBPACK_IMPORTED_MODULE_1__["AppService"]],
        }),
        __metadata("design:paramtypes", [_app_service__WEBPACK_IMPORTED_MODULE_1__["AppService"], _register_service__WEBPACK_IMPORTED_MODULE_2__["RegisterService"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/logout.service.ts":
/*!***********************************!*\
  !*** ./src/app/logout.service.ts ***!
  \***********************************/
/*! exports provided: LogoutService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LogoutService", function() { return LogoutService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/esm5/dialog.es5.js");
/* harmony import */ var _logoutdialog_logoutdialog_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./logoutdialog/logoutdialog.component */ "./src/app/logoutdialog/logoutdialog.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LogoutService = /** @class */ (function () {
    function LogoutService(router, dialog) {
        this.router = router;
        this.dialog = dialog;
    }
    LogoutService.prototype.openDialog = function (diagTitle, diagText, okButtonText) {
        var _this = this;
        if (diagTitle === void 0) { diagTitle = "You have been logged out"; }
        if (diagText === void 0) { diagText = "Session ended - you will be redirected to login"; }
        if (okButtonText === void 0) { okButtonText = "Proceed to login"; }
        var dialogRef = this.dialog.open(_logoutdialog_logoutdialog_component__WEBPACK_IMPORTED_MODULE_3__["LogoutdialogComponent"]);
        var instance = dialogRef.componentInstance;
        instance.dialogTitle = diagTitle;
        instance.dialogText = diagText;
        instance.okButtonText = okButtonText;
        dialogRef.afterClosed().subscribe(function (result) {
            console.log("Dialog result: " + result);
            if (result == true) {
                _this.router.navigate(['login']);
            }
        });
    };
    LogoutService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _angular_material_dialog__WEBPACK_IMPORTED_MODULE_2__["MatDialog"]])
    ], LogoutService);
    return LogoutService;
}());



/***/ }),

/***/ "./src/app/logoutdialog/logoutdialog.component.css":
/*!*********************************************************!*\
  !*** ./src/app/logoutdialog/logoutdialog.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/logoutdialog/logoutdialog.component.html":
/*!**********************************************************!*\
  !*** ./src/app/logoutdialog/logoutdialog.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<h2 mat-dialog-title></h2>\n<mat-dialog-content class=\"mat-typography\">\n  <h3>{{dialogTitle}}</h3>\n  <p>\n      {{dialogText}}\n  </p>\n  \n</mat-dialog-content>\n<mat-dialog-actions align=\"end\">\n  <!-- <button mat-button mat-dialog-close>Cancel</button> -->\n  <button mat-button [mat-dialog-close]=\"true\" cdkFocusInitial>{{okButtonText}}</button>\n</mat-dialog-actions>"

/***/ }),

/***/ "./src/app/logoutdialog/logoutdialog.component.ts":
/*!********************************************************!*\
  !*** ./src/app/logoutdialog/logoutdialog.component.ts ***!
  \********************************************************/
/*! exports provided: LogoutdialogComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LogoutdialogComponent", function() { return LogoutdialogComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

// Session ended - you will be redirected to login
//       <br>
//       Possible causes: token expired / server timeout
var LogoutdialogComponent = /** @class */ (function () {
    function LogoutdialogComponent() {
    }
    LogoutdialogComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], LogoutdialogComponent.prototype, "dialogTitle", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], LogoutdialogComponent.prototype, "dialogText", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], LogoutdialogComponent.prototype, "okButtonText", void 0);
    LogoutdialogComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-logoutdialog',
            template: __webpack_require__(/*! ./logoutdialog.component.html */ "./src/app/logoutdialog/logoutdialog.component.html"),
            styles: [__webpack_require__(/*! ./logoutdialog.component.css */ "./src/app/logoutdialog/logoutdialog.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], LogoutdialogComponent);
    return LogoutdialogComponent;
}());



/***/ }),

/***/ "./src/app/materialm/materialm.module.ts":
/*!***********************************************!*\
  !*** ./src/app/materialm/materialm.module.ts ***!
  \***********************************************/
/*! exports provided: MaterialmModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MaterialmModule", function() { return MaterialmModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var MaterialmModule = /** @class */ (function () {
    function MaterialmModule() {
    }
    MaterialmModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDatepickerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatNativeDateModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatInputModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_2__["BrowserAnimationsModule"]
            ],
            exports: [
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDatepickerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatNativeDateModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatInputModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_2__["BrowserAnimationsModule"]
            ],
            providers: [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDatepickerModule"]],
        })
    ], MaterialmModule);
    return MaterialmModule;
}());



/***/ }),

/***/ "./src/app/register.service.ts":
/*!*************************************!*\
  !*** ./src/app/register.service.ts ***!
  \*************************************/
/*! exports provided: RegisterService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterService", function() { return RegisterService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var RegisterService = /** @class */ (function () {
    function RegisterService(httpClient) {
        this.httpClient = httpClient;
    }
    // http://localhost:9333/register/newuser
    RegisterService.prototype.register = function (registerData) {
        // let requestJson = {
        //   username: username,
        //   pssword: password,
        //   email: email
        // };
        return this.httpClient.post(_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].contextPathServer + "/register/newuser", registerData);
    };
    RegisterService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], RegisterService);
    return RegisterService;
}());



/***/ }),

/***/ "./src/app/search/search.component.css":
/*!*********************************************!*\
  !*** ./src/app/search/search.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".mat-option{\r\n    width: 400px;\r\n}\r\n\r\n.mat-autocomplete-panel{\r\n    min-width: 500px;\r\n    width: 500px;\r\n}"

/***/ }),

/***/ "./src/app/search/search.component.html":
/*!**********************************************!*\
  !*** ./src/app/search/search.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- Search -->\n<form class=\"d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search\">\n  <mat-form-field>\n    \n\n    <div class=\"input-group\">\n      <input  matInput [(ngModel)]=\"searchText\" (keyup)=\"doSearch($event.target.value)\" class=\"form-control bg-light border-0 \"\n        aria-label=\"Search\" aria-describedby=\"basic-addon2\" [formControl]=\"myControl\" [matAutocomplete]=\"auto\">\n      <div class=\"input-group-append\">\n        <button class=\"btn btn-primary\" type=\"button\">\n          <i class=\"fas fa-search fa-sm\"></i>\n        </button>\n      </div>\n    </div>\n    <mat-autocomplete #auto=\"matAutocomplete\" style=\"min-width: 350px;\">\n\n      <mat-option *ngFor=\"let farm of foundFarms\" [value]=\"farm.farmName\">\n        <a [routerLink]=\"['/farms', farm.id]\">{{farm.farmName}} <strong>in Farms</strong></a>\n      </mat-option>\n\n      <mat-option *ngFor=\"let farmSector of foundFarmSectors\" [value]=\"farmSector.sectorDescription\">\n        <a [routerLink]=\"['/farms/sectors', farmSector.id]\">{{farmSector.sectorDescription}} <strong> in Sectors</strong></a>\n      </mat-option>\n      \n\n    </mat-autocomplete>\n  </mat-form-field>\n</form>"

/***/ }),

/***/ "./src/app/search/search.component.ts":
/*!********************************************!*\
  !*** ./src/app/search/search.component.ts ***!
  \********************************************/
/*! exports provided: SearchComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchComponent", function() { return SearchComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _farms_farmsrv_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../farms/farmsrv.service */ "./src/app/farms/farmsrv.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SearchComponent = /** @class */ (function () {
    function SearchComponent(farmService) {
        this.farmService = farmService;
        this.allFarms = [];
        this.allFarmSectors = [];
        this.searchText = '';
        this.foundFarms = [];
        this.foundFarmSectors = [];
        this.myControl = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]();
    }
    SearchComponent.prototype.ngOnInit = function () {
        this.loadInformationForSearch();
    };
    SearchComponent.prototype.loadInformationForSearch = function () {
        var _this = this;
        this.farmService.findAllFarms().subscribe(function (rez) {
            _this.allFarms = rez.json();
            console.log('ALL FARMS: ', _this.allFarms);
        }, function (err) {
            // TODO: check if logged in
            console.log(err);
        });
        this.farmService.findAllSectors().subscribe(function (rez) {
            _this.allFarmSectors = rez.json();
            // this.foundFarmSectors = [...this.allFarmSectors];
            console.log('ALL FARM SECTORS: ', _this.allFarmSectors);
        }, function (err) {
            console.log(err);
        });
    };
    SearchComponent.prototype.doSearch = function (searchTerm) {
        if (localStorage.getItem('loggedin')) {
            localStorage.removeItem('loggedin');
            this.loadInformationForSearch();
        }
        console.log('searching for: ', searchTerm);
        this.foundFarms = this.allFarms.slice();
        this.foundFarmSectors = this.allFarmSectors.slice();
        this.foundFarms = this.foundFarms.filter(function (x) { return x.farmName.toUpperCase().indexOf(searchTerm.toUpperCase()) != -1; });
        this.foundFarmSectors = this.foundFarmSectors.filter(function (x) { return x.sectorDescription.toUpperCase().indexOf(searchTerm.toUpperCase()) != -1; });
    };
    SearchComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-search',
            template: __webpack_require__(/*! ./search.component.html */ "./src/app/search/search.component.html"),
            styles: [__webpack_require__(/*! ./search.component.css */ "./src/app/search/search.component.css")]
        }),
        __metadata("design:paramtypes", [_farms_farmsrv_service__WEBPACK_IMPORTED_MODULE_1__["FarmsrvService"]])
    ], SearchComponent);
    return SearchComponent;
}());



/***/ }),

/***/ "./src/app/user.service.ts":
/*!*********************************!*\
  !*** ./src/app/user.service.ts ***!
  \*********************************/
/*! exports provided: UserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return UserService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var UserService = /** @class */ (function () {
    function UserService(http) {
        this.http = http;
    }
    UserService.prototype.appendToken = function () {
        return "?access_token=" + localStorage.getItem('access_token');
    };
    UserService.prototype.getPrincipalDto = function () {
        return this.http.get(_environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].contextPathServer + "/rest/security/current-user" + this.appendToken());
    };
    UserService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], UserService);
    return UserService;
}());



/***/ }),

/***/ "./src/app/user/user.component.css":
/*!*****************************************!*\
  !*** ./src/app/user/user.component.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/user/user.component.html":
/*!******************************************!*\
  !*** ./src/app/user/user.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  user works!\n</p>\n"

/***/ }),

/***/ "./src/app/user/user.component.ts":
/*!****************************************!*\
  !*** ./src/app/user/user.component.ts ***!
  \****************************************/
/*! exports provided: UserComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserComponent", function() { return UserComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../app.service */ "./src/app/app.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var UserComponent = /** @class */ (function () {
    function UserComponent(_service) {
        this._service = _service;
        this.url = 'http://localhost:9333/rest/users/all';
    }
    UserComponent.prototype.ngOnInit = function () {
    };
    UserComponent.prototype.getFoo = function () {
        this._service.getResource(this.url);
    };
    UserComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-user',
            template: __webpack_require__(/*! ./user.component.html */ "./src/app/user/user.component.html"),
            styles: [__webpack_require__(/*! ./user.component.css */ "./src/app/user/user.component.css")],
            providers: [_app_service__WEBPACK_IMPORTED_MODULE_1__["AppService"]],
        }),
        __metadata("design:paramtypes", [_app_service__WEBPACK_IMPORTED_MODULE_1__["AppService"]])
    ], UserComponent);
    return UserComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false,
    API_KEY_GOOOGLE: 'AIzaSyDyJRQ6n7i9c2ncr03yVaiDs6l45B1GTu8',
    contextPathServer: '',
    contextPathDevelopment: 'http://localhost:9333'
    // contextPathServer : 'https://floating-fortress-10026.herokuapp.com'
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\STUDENT\Desktop\LicentaDev\frontend\farmang\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map