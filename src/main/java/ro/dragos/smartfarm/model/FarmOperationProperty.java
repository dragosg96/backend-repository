package ro.dragos.smartfarm.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the farm_operation_property database table.
 * 
 */
@Entity
@Table(name="farm_operation_property")
@NamedQuery(name="FarmOperationProperty.findAll", query="SELECT f FROM FarmOperationProperty f")
public class FarmOperationProperty implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="property_name")
	private String propertyName;

	@Column(name="property_value")
	private String propertyValue;

	//bi-directional many-to-one association to FarmOperation
	@ManyToOne
	@JoinColumn(name="farm_operation_id")
	private FarmOperation farmOperation;

	//bi-directional many-to-one association to FarmSectorDetail
//	@OneToMany(mappedBy="farmOperationProperty")
//	private List<FarmSectorDetail> farmSectorDetails;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="date_applied")
	private Date dateApplied;
	
	@Column(name="historic_weather")
	private String historicWeather;
	
	public FarmOperationProperty() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPropertyName() {
		return this.propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getPropertyValue() {
		return this.propertyValue;
	}

	public void setPropertyValue(String propertyValue) {
		this.propertyValue = propertyValue;
	}

	public FarmOperation getFarmOperation() {
		return this.farmOperation;
	}

	public void setFarmOperation(FarmOperation farmOperation) {
		this.farmOperation = farmOperation;
	}

	
	
	public Date getDateApplied() {
		return dateApplied;
	}

	public void setDateApplied(Date dateApplied) {
		this.dateApplied = dateApplied;
	}
	
	

	public String getHistoricWeather() {
		return historicWeather;
	}

	public void setHistoricWeather(String historicWeather) {
		this.historicWeather = historicWeather;
	}

	@Override
	public String toString() {
		return "FarmOperationProperty [id=" + id + ", propertyName=" + propertyName + ", propertyValue=" + propertyValue
				+ "]";
	}

//	public List<FarmSectorDetail> getFarmSectorDetails() {
//		return this.farmSectorDetails;
//	}
//
//	public void setFarmSectorDetails(List<FarmSectorDetail> farmSectorDetails) {
//		this.farmSectorDetails = farmSectorDetails;
//	}

//	public FarmSectorDetail addFarmSectorDetail(FarmSectorDetail farmSectorDetail) {
//		getFarmSectorDetails().add(farmSectorDetail);
//		farmSectorDetail.setFarmOperationProperty(this);
//
//		return farmSectorDetail;
//	}
//
//	public FarmSectorDetail removeFarmSectorDetail(FarmSectorDetail farmSectorDetail) {
//		getFarmSectorDetails().remove(farmSectorDetail);
//		farmSectorDetail.setFarmOperationProperty(null);
//
//		return farmSectorDetail;
//	}
	
	

}