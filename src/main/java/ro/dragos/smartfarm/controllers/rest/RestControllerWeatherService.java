package ro.dragos.smartfarm.controllers.rest;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ro.dragos.smartfarm.dto.DTOWeather;
import ro.dragos.smartfarm.service.WeatherService;

@RestController
@RequestMapping("/rest/weather")
@CrossOrigin("http://localhost:4200")
public class RestControllerWeatherService {

	
	
	
	
	@GetMapping("/current/{lat}/{lng}")
	public DTOWeather testWeatherService(@PathVariable("lat") double lat, @PathVariable("lng") double lng){
		return WeatherService.loadFromEndpoint(lat, lng);
	}
	
}
