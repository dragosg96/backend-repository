package ro.dragos.smartfarm.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ro.dragos.smartfarm.dao.RepoSoilType;
import ro.dragos.smartfarm.model.SoilType;

@RestController
@RequestMapping("/rest/soils")
public class RestControllerSoilType {

	@Autowired
	private RepoSoilType repoSoil;
	
	@RequestMapping("/all")
	public Iterable<SoilType> allSoils(){
		return repoSoil.findAll();
	}
}
