package ro.dragos.smartfarm.config;

public enum USER_TYPES {
	SUPER_ADMIN, FARM_OWNER
}
