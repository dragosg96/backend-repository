package ro.dragos.smartfarm.config;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import ro.dragos.smartfarm.dao.RepoUser;

@Service
public class DefaultUserDetailsService implements UserDetailsService {

	
	@Autowired
	private RepoUser repoUser;
	
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return findRealUser(username);
    }

    
    private UserDetails findRealUser(String username){
    	if(repoUser.findByUsername(username).isPresent()){
    		ro.dragos.smartfarm.model.User userDb = repoUser.findByUsername(username).get();
    		 UserDetails userSpringBootOauth = User.withDefaultPasswordEncoder()
    	                .username(userDb.getUsername())
    	                .password(userDb.getPassword())
    	                .authorities(getAuthority())
    	                .build();
    		return userSpringBootOauth;
    	}else{
    		return null;
    	}
    }
    
    private UserDetails mockUser(String username) {
    	
    	
    	
        String userName = "test@test.com";
        String userPass = "tester";

        if (!userName.equals(username)) {
            throw new UsernameNotFoundException("Invalid username or password.");
        }

        // this is another way of dealing with password encoding
        // password will be stored in bcrypt in this example
        // you can also use a prefix, @see com.patternmatch.oauth2blog.config.AuthorizationServerConfig#CLIENT_SECRET
        UserDetails user = User.withDefaultPasswordEncoder()
                .username(username)
                .password(userPass)
                .authorities(getAuthority())
                .build();

        return user;
    }

    private List<SimpleGrantedAuthority> getAuthority() {
        return Collections.emptyList();
    }
}