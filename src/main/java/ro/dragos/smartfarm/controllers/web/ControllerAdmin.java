package ro.dragos.smartfarm.controllers.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/web/admin")
public class ControllerAdmin {

	@RequestMapping("/home")
	public String adminPage(){
		return "administration";
	}
}
