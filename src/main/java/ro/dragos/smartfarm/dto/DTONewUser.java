package ro.dragos.smartfarm.dto;

public class DTONewUser {

	
	private String email;
	private String username;
	private boolean operationSuccessful;
	private String operationMessage;
	
	public DTONewUser() {
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	
	
	public boolean isOperationSuccessful() {
		return operationSuccessful;
	}

	public void setOperationSuccessful(boolean operationSuccessful) {
		this.operationSuccessful = operationSuccessful;
	}

	public String getOperationMessage() {
		return operationMessage;
	}

	public void setOperationMessage(String operationMessage) {
		this.operationMessage = operationMessage;
	}

	@Override
	public String toString() {
		return "DTONewUser [email=" + email + ", username=" + username + "]";
	}
	
	
	
}
