package ro.dragos.smartfarm.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the soil_types database table.
 * 
 */
@Entity
@Table(name="soil_types")
@NamedQuery(name="SoilType.findAll", query="SELECT s FROM SoilType s")
public class SoilType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	private String name;



	public SoilType() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	

}