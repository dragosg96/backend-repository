package ro.dragos.smartfarm.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ro.dragos.smartfarm.model.FarmOperationProperty;

@Repository
public interface RepoOperationProperty extends CrudRepository<FarmOperationProperty, Integer>{

	
	
	
}
