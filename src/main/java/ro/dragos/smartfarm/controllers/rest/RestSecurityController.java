package ro.dragos.smartfarm.controllers.rest;

import java.security.Principal;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ro.dragos.smartfarm.dao.RepoUser;
import ro.dragos.smartfarm.dto.DTOSecurity;
import ro.dragos.smartfarm.model.User;

@RestController
@RequestMapping("/rest/security")
@CrossOrigin("http://localhost:4200")
public class RestSecurityController {

	
	@Autowired
	private RepoUser repoUser;
	
	@GetMapping("/current-user")
	public DTOSecurity getCurrentPrincipalDetail(Principal principal){
		DTOSecurity dto = new DTOSecurity();
		dto.setUsername(principal.getName());
		Optional<User> userFound = repoUser.findByUsername(principal.getName());
		if(userFound.isPresent()){
			User actualUser = userFound.get();
			dto.setUserType(actualUser.getUserType());
		}
		
		return dto;
	}
	
}
