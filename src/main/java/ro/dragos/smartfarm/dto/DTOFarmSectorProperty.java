package ro.dragos.smartfarm.dto;

public class DTOFarmSectorProperty {

	// JSON object properties:
//	farmSectorId: 1
//	id: 1
//	propName: "humidity 2"
//	propValue: "300"
	
	private Integer farmSectorPropertyId; // for update
	private Integer farmSectorId;
	private String propertyName;
	private String propertyValue;
	
	
	public DTOFarmSectorProperty() {
	}

	public DTOFarmSectorProperty(Integer farmSectorId, String propertyName, String propertyValue) {
		this.farmSectorId = farmSectorId;
		this.propertyName = propertyName;
		this.propertyValue = propertyValue;
	}
	
	

	public DTOFarmSectorProperty(Integer farmSectorPropertyId, Integer farmSectorId, String propertyName,
			String propertyValue) {
		this.farmSectorPropertyId = farmSectorPropertyId;
		this.farmSectorId = farmSectorId;
		this.propertyName = propertyName;
		this.propertyValue = propertyValue;
	}

	public Integer getFarmSectorId() {
		return farmSectorId;
	}

	public void setFarmSectorId(Integer farmSectorId) {
		this.farmSectorId = farmSectorId;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getPropertyValue() {
		return propertyValue;
	}

	public void setPropertyValue(String propertyValue) {
		this.propertyValue = propertyValue;
	}

	public Integer getFarmSectorPropertyId() {
		return farmSectorPropertyId;
	}

	public void setFarmSectorPropertyId(Integer farmSectorPropertyId) {
		this.farmSectorPropertyId = farmSectorPropertyId;
	}

	@Override
	public String toString() {
		return "DTOFarmSectorProperty [farmSectorPropertyId=" + farmSectorPropertyId + ", farmSectorId=" + farmSectorId
				+ ", propertyName=" + propertyName + ", propertyValue=" + propertyValue + "]";
	}

	

	
	
	
}
