package ro.dragos.smartfarm.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ro.dragos.smartfarm.model.SoilType;

@Repository
public interface RepoSoilType extends CrudRepository<SoilType, Integer>{

}
