package ro.dragos.smartfarm.controllers.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ro.dragos.smartfarm.dao.RepoInterval;
import ro.dragos.smartfarm.dao.RepoOperations;
import ro.dragos.smartfarm.dto.DTOInterval;
import ro.dragos.smartfarm.model.FarmOperation;
import ro.dragos.smartfarm.model.OpInterval;

@RestController
@RequestMapping("/rest/interval")
public class RestControllerInterval {

	@Autowired
	private RepoInterval repoInterval;
	
	@Autowired
	private RepoOperations repoOperations;
	
	@GetMapping("/all")
	public Iterable<OpInterval> findAll(){
		return repoInterval.findAll();
	}
	
	@GetMapping("/bysectorid/{sectorid}")
	public List<OpInterval> findBySectorId(@PathVariable("sectorid") Integer sectorId){
		return repoInterval.findByPkIdSector(sectorId);
	}
	
	
	@GetMapping("/dtobysectorid/{sectorid}")
	public List<DTOInterval> findIntervalAndFarmSectorBySectorId(@PathVariable("sectorid") Integer sectorId){
		List<OpInterval> intervals = repoInterval.findByPkIdSector(sectorId);
		List<DTOInterval> dtos = new ArrayList<>();
		
		for(OpInterval interval : intervals){
			DTOInterval dto = new DTOInterval();
			dto.setInterval(interval);
			FarmOperation foperation = repoOperations.findById(interval.getPk().getIdOperation()).get();
			dto.setOperation(foperation);
			
			dtos.add(dto);
		}
		
		return dtos;
	}
	
	@PostMapping("/save")
	public OpInterval saveInterval(@RequestBody OpInterval newInterval){
		repoInterval.save(newInterval);
		return newInterval;
	}
	
}
