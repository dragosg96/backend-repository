package ro.dragos.smartfarm.service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpHelper {

	public static String load(String sourceUrl) {

		String resultString = "";

		try {

			URL url = new URL(sourceUrl);
			HttpURLConnection con;
			con = (HttpURLConnection) url.openConnection();
			InputStream in = con.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String serverLine = br.readLine();
			resultString += serverLine;
			while (serverLine != null) {

				serverLine = br.readLine();
				if (serverLine != null) {

					resultString += serverLine;

				}

			}

		} catch (Exception e) {

			e.printStackTrace();

		}

		return resultString;

	}
	
//	public static void main(String[] args) {
//		
//		HttpHelper helper = new HttpHelper();
//		String download = helper.load("http://api.openweathermap.org/data/2.5/weather?q=Bucharest&appid=56a2ac50d2fbb400335612e7d8e41a5d");
//		System.out.println("DOWNLOADED: " + download);
//	}

}
