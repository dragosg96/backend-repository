package ro.dragos.smartfarm.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ro.dragos.smartfarm.model.Address;

@Repository
public interface RepoAddress extends CrudRepository<Address, Integer>{

}
