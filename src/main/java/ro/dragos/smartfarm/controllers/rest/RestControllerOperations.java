package ro.dragos.smartfarm.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ro.dragos.smartfarm.dao.RepoOperations;
import ro.dragos.smartfarm.model.FarmOperation;

@RequestMapping("/rest/operations")
@RestController
@CrossOrigin("http://localhost:4200")
public class RestControllerOperations {

	@Autowired
	private RepoOperations repoOperations;
	
	@RequestMapping("/all")
	public Iterable<FarmOperation> findAll(){
		return repoOperations.findAll();
	}
}
