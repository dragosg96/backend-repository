package ro.dragos.smartfarm.controllers.rest;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ro.dragos.smartfarm.dao.RepoFarmSectorDetail;
import ro.dragos.smartfarm.dao.RepoOperationProperty;
import ro.dragos.smartfarm.dao.RepoSectorProperty;
import ro.dragos.smartfarm.dto.DTOIrigation;
import ro.dragos.smartfarm.dto.DTOPolygon;
import ro.dragos.smartfarm.dto.DTOWeather;
import ro.dragos.smartfarm.model.FarmOperationProperty;
import ro.dragos.smartfarm.model.FarmSector;
import ro.dragos.smartfarm.model.FarmSectorDetail;
import ro.dragos.smartfarm.model.FarmSectorProperty;
import ro.dragos.smartfarm.service.DateService;
import ro.dragos.smartfarm.service.PolygonService;

@RestController
@RequestMapping("/rest/irigation")
public class RestControllerIrigation {

	@Autowired
	private RepoOperationProperty repoOperationProperty;

	@Autowired
	private RepoFarmSectorDetail repoFarmSectorDetail;
	
	@Autowired
	private RepoSectorProperty repoFarmSectorProperty;
	
	@GetMapping("/bydate/{year}/{month}/{day}")
	public List<FarmSectorProperty> findByStartDateBetween(@PathVariable("year") int year, 
				@PathVariable("month") int month,
				@PathVariable("day") int day){
		Date dateStart = DateService.getSomedayMorning(year, month, day);
		Date dateEnd = DateService.getSomedayEvening(year, month, day);
//		+----+-------------+------------+----------------+---------------------+
//		| 15 | umiditate   | 100        |              1 | 2019-05-10 12:37:40 |
//		| 16 | umiditate   | 300        |              1 | 2019-05-29 12:37:40 |
//		| 17 | temperatura | 20         |              1 | 2019-05-29 12:37:40 |
		System.out.println("------- DATE START: " + dateStart + " ---- DATE END: " + dateEnd);
		List<FarmSectorProperty> propertiesForInterval = repoFarmSectorProperty.findByDateMeasuredBetween(dateStart, dateEnd);
		return propertiesForInterval;
		
	}
	
	@GetMapping("/byid/{id}")
	public DTOIrigation findOperationPropertyInfo(@PathVariable("id") Integer id) {
		DTOIrigation dto = new DTOIrigation();
		FarmOperationProperty operationProperty = repoOperationProperty.findById(id).get();
		dto.setFarmOperationProperty(operationProperty);
		
		String historicWeather = operationProperty.getHistoricWeather();
		if (historicWeather != null) {
			try {
				ObjectMapper mapper = new ObjectMapper();
				DTOWeather dtoWeather = (DTOWeather) mapper.readValue(historicWeather, DTOWeather.class);
				dto.setDtoWeather(dtoWeather);
			} catch (JsonParseException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
		
		List<FarmSectorDetail> fsdAssociatedWithOperation = repoFarmSectorDetail.findByFarmOperationProperty(operationProperty);
		if(fsdAssociatedWithOperation.size() > 0){
			// there is a farmSectorDetail associated with this operation
			FarmSectorDetail fsd = fsdAssociatedWithOperation.get(0);
			FarmSector sector = fsd.getFarmSector();
			System.out.println("INVOKED");
			
			
			dto.setFarmSectorId(sector.getId());
			
			if (sector.getCoordinates() != null) {
				DTOPolygon dtoPolygon = PolygonService.string2Polygon(sector.getCoordinates());
				// sector.setDtoPolygon(dtoPolygon);
				dto.setDtoPolygon(dtoPolygon);
				
			}
			dto.setSoilType(sector.getSoilTypeBean());
			
		}
		
		
		
		return dto;
	}

}
