package ro.dragos.smartfarm.dao;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ro.dragos.smartfarm.model.User;

@Repository
public interface RepoUser extends CrudRepository<User, Integer>{

	
	public Optional<User> findByUsername(String username);
	public Optional<User> findByUsernameAndEmail(String username, String email);
	
	
}
