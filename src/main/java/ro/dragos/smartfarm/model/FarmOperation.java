package ro.dragos.smartfarm.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the farm_operation database table.
 * 
 */
@Entity
@Table(name="farm_operation")
@NamedQuery(name="FarmOperation.findAll", query="SELECT f FROM FarmOperation f")
public class FarmOperation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="operation_name")
	private String operationName;

	//bi-directional many-to-one association to FarmOperationProperty
//	@OneToMany(mappedBy="farmOperation")
//	private List<FarmOperationProperty> farmOperationProperties;

	public FarmOperation() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getOperationName() {
		return this.operationName;
	}

	public void setOperationName(String operationName) {
		this.operationName = operationName;
	}

//	public List<FarmOperationProperty> getFarmOperationProperties() {
//		return this.farmOperationProperties;
//	}
//
//	public void setFarmOperationProperties(List<FarmOperationProperty> farmOperationProperties) {
//		this.farmOperationProperties = farmOperationProperties;
//	}

//	public FarmOperationProperty addFarmOperationProperty(FarmOperationProperty farmOperationProperty) {
//		getFarmOperationProperties().add(farmOperationProperty);
//		farmOperationProperty.setFarmOperation(this);
//
//		return farmOperationProperty;
//	}
//
//	public FarmOperationProperty removeFarmOperationProperty(FarmOperationProperty farmOperationProperty) {
//		getFarmOperationProperties().remove(farmOperationProperty);
//		farmOperationProperty.setFarmOperation(null);
//
//		return farmOperationProperty;
//	}

}