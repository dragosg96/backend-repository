package ro.dragos.smartfarm.controllers.rest;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ro.dragos.smartfarm.config.IConfigConstants;
import ro.dragos.smartfarm.config.USER_TYPES;
import ro.dragos.smartfarm.dao.RepoAddress;
import ro.dragos.smartfarm.dao.RepoFarm;
import ro.dragos.smartfarm.dao.RepoFarmSector;
import ro.dragos.smartfarm.dao.RepoFarmSectorDetail;
import ro.dragos.smartfarm.dao.RepoOperationProperty;
import ro.dragos.smartfarm.dao.RepoSectorProperty;
import ro.dragos.smartfarm.dto.DTOFarmSectorProperty;
import ro.dragos.smartfarm.dto.DTOPolygon;
import ro.dragos.smartfarm.dto.DTOSaveFarm;
import ro.dragos.smartfarm.dto.DTOWeather;
import ro.dragos.smartfarm.model.Address;
import ro.dragos.smartfarm.model.Farm;
import ro.dragos.smartfarm.model.FarmOperation;
import ro.dragos.smartfarm.model.FarmOperationProperty;
import ro.dragos.smartfarm.model.FarmSector;
import ro.dragos.smartfarm.model.FarmSectorDetail;
import ro.dragos.smartfarm.model.FarmSectorProperty;
import ro.dragos.smartfarm.model.User;
import ro.dragos.smartfarm.service.PolygonService;
import ro.dragos.smartfarm.service.SecurityService;
import ro.dragos.smartfarm.service.WeatherService;

@RestController
@RequestMapping("/rest/farms")
@CrossOrigin("http://localhost:4200")
public class RestControllerFarms {

	@Autowired
	private RepoFarm repoFarm;
	@Autowired
	private RepoFarmSector repoFarmSector;
	@Autowired
	private RepoSectorProperty repoSectorProperty;
	@Autowired
	private RepoFarmSectorDetail repoFarmSectorDetail;
	@Autowired
	private RepoOperationProperty repoOperationProperty;

	@Autowired
	private RepoAddress repoAddress;

	// @Autowired
	// private RepoUser repoUser;
	//
	//
	// private User findLoggedUserFromPrincipal(Principal principal){
	// User loggedInUser = repoUser.findByUsername(principal.getName()).get();
	// return loggedInUser;
	// }

	@Autowired
	private SecurityService securityService;
	//

	/**
	 * This method returns all the farms, only if the currently logged in user
	 * has admin rights. Otherwise, the principal of the logged in user is used
	 * in order to filter the farms according to the relationship between farms
	 * and the current user ownership level
	 * 
	 * @return
	 */
	@RequestMapping("/farm/all")
	public Iterable<Farm> allFarms(Principal principal) {

		Iterable<Farm> allFarms = repoFarm.findAll();
		User loggedInUser = securityService.findLoggedUserFromPrincipal(principal);
		System.out.println("CURRENT USER LOGGED IN: " + loggedInUser.getUserType());
		boolean allFarmsAccess = loggedInUser.getUserType().toString().equals(USER_TYPES.SUPER_ADMIN.toString());
		System.out.println("SHOULD ACCESS ALL FARMS? " + allFarmsAccess);
		if (loggedInUser.getUserType().toString().equals(USER_TYPES.SUPER_ADMIN.toString())) {
			// user is admin, can see al farms
			return allFarms;
		}

		List<Farm> farmsForUser = new ArrayList<>();
		for (Farm farm : allFarms) {
			if (farm.getUser().equals(loggedInUser)) {
				farmsForUser.add(farm);
			}
		}
		return farmsForUser;
	}

	@PostMapping("/save")
	public Farm saveFarm(@RequestBody DTOSaveFarm dto, Principal principal) {
		Address newAddress = new Address();
		newAddress.setCity(dto.getCity());
		newAddress.setStreet(dto.getStreet());
		newAddress.setsNumber(dto.getNumber());

		Farm newFarm = new Farm();
		newFarm.setAddress(newAddress);
		newFarm.setFarmName(dto.getFarmName());

		User loggedInUser = securityService.findLoggedUserFromPrincipal(principal);

		newFarm.setUser(loggedInUser);

		repoFarm.save(newFarm);

		return newFarm;
	}

	@GetMapping("/farm/byid/{id}")
	public ResponseEntity<Farm> findById(@PathVariable("id") Integer id, Principal principal) {
		User loggedInUser = securityService.findLoggedUserFromPrincipal(principal);
		System.out.println("** USER LOGGED IN: " + principal.getName());
		System.out.println("** USER LOGGED IN TYPE: " + loggedInUser.getUserType());
		boolean shouldViewFarm = loggedInUser.getUserType().toString().equals(USER_TYPES.SUPER_ADMIN.toString());
		System.out.println("** ACCESS TO FARM INFO: " + shouldViewFarm);
		if (loggedInUser.getUserType().toString().equals(USER_TYPES.SUPER_ADMIN.toString())) {
			Farm theFarm = repoFarm.findById(id).get();
			return ResponseEntity.ok(theFarm);
		}
		Farm theFarm = repoFarm.findById(id).get();
		if (theFarm.getUser().equals(loggedInUser)) {
			// okay
			return ResponseEntity.ok(theFarm);
		}

		// throw new RuntimeException("");
		ResponseEntity badResponse = ResponseEntity.status(HttpStatus.FORBIDDEN).build();
		return badResponse;

	}

	@DeleteMapping("/farm/delete/{id}")
	public Iterable<Farm> deleteById(@PathVariable("id") Integer id) {
		Farm farm = repoFarm.findById(id).get();
		repoFarm.delete(farm);
		return repoFarm.findAll();
	}

	@RequestMapping("/sectors/all")
	public Iterable<FarmSector> allFarmSectors(Principal principal) {
		User loggedInUser = securityService.findLoggedUserFromPrincipal(principal);
		if (loggedInUser.getUserType().toString().equals(USER_TYPES.SUPER_ADMIN.toString())) {
			// user is admin, can see al farms
			return repoFarmSector.findAll();
		}

		List<FarmSector> sectorsForUser = new ArrayList<>();
		for (FarmSector fs : repoFarmSector.findAll()) {
			if (fs.getFarm().getUser().getId() == loggedInUser.getId()) {
				sectorsForUser.add(fs);
			}
		}
		return sectorsForUser;
	}

	@RequestMapping("/sectors/byid/{idsector}")
	public FarmSector findSectorById(@PathVariable("idsector") int idSector, Principal principal) {
		FarmSector sector = repoFarmSector.findById(idSector).get();

		if (sector.getCoordinates() != null) {
			DTOPolygon dtoPolygon = PolygonService.string2Polygon(sector.getCoordinates());
			sector.setDtoPolygon(dtoPolygon);
			DTOWeather dto = WeatherService.loadFromEndpoint(sector.getDtoPolygon().getCoordinates().get(0).getLat(),
					sector.getDtoPolygon().getCoordinates().get(0).getLng());
			sector.setDtoWeather(dto);
		}

		return sector;
	}

	@PutMapping("/sectors/updatecoordnull/{idsector}")
	public FarmSector updateSectorSetCoordinatesNull(@PathVariable("idsector") int idSector) {
		FarmSector sector = repoFarmSector.findById(idSector).get();
		sector.setCoordinates(null);
		sector.setDtoPolygon(null);
		repoFarmSector.save(sector);

		// when setting polygon coordinates and coordinates string
		// to null, there is no need to call coordinates conversion methods
		FarmSector updated = repoFarmSector.findById(idSector).get();
		return updated;
	}

	/**
	 * Update used in order to set coordinates
	 * 
	 * @param idSector
	 * @param farmSector
	 * @return
	 */
	@PutMapping("/sectors/update/{idsector}")
	public FarmSector updateSector(@PathVariable("idsector") int idSector, @RequestBody FarmSector farmSector) {
		FarmSector sector = repoFarmSector.findById(idSector).get(); // old data

		// -32.0849631642692,146.82790096586268,-31.767934263262863,148.26710995023768,-33.0846988425446,148.16823299711268,-33.80431611819372,148.93178280180018,-32.2709391778134,146.98170955961268,-33.69012747976644,145.79518612211268,-32.289515876069764,145.4875689346124

		String coordinates = PolygonService.polygon2String(farmSector.getDtoPolygon());
		sector.setCoordinates(coordinates);

		System.out.println("SERVER PARAM: " + farmSector.getDtoPolygon().getCoordinates()); // TODO:
																							// check
																							// for
																							// null
		System.out.println("SERVER RESULT: " + sector.getCoordinates());
		repoFarmSector.save(sector);

		DTOPolygon dtoPolygon = PolygonService.string2Polygon(coordinates);
		sector.setDtoPolygon(dtoPolygon);

		// if(sector.getCoordinates() != null){
		// DTOPolygon dtoPolygon =
		// PolygonService.string2Polygon(sector.getCoordinates());
		// sector.setDtoPolygon(dtoPolygon);
		// }
		return sector;
	}

	@PostMapping("/sectors/add/{idfarm}")
	public Iterable<FarmSector> addFarmSector(@PathVariable("idfarm") Integer idfarm,
			@RequestBody FarmSector farmSector) {

		Farm farm = repoFarm.findById(idfarm).get();
		farmSector.setFarm(farm);
		repoFarmSector.save(farmSector);
		List<FarmSector> farmSectorsForSelectedFarm = repoFarmSector.findByFarm(farm);
		return farmSectorsForSelectedFarm;
	}

	@PutMapping("/sectors/properties/update")
	public FarmSector updatePropertyOfFarmSector(@RequestBody DTOFarmSectorProperty dto) {
		FarmSector farmSector = repoFarmSector.findById(dto.getFarmSectorId()).get();
		FarmSectorProperty farmSectorProperty = new FarmSectorProperty();
		farmSectorProperty.setPropName(dto.getPropertyName());
		farmSectorProperty.setPropValue(dto.getPropertyValue());
		farmSectorProperty.setId(dto.getFarmSectorPropertyId());

		FarmSectorProperty existingProperty = repoSectorProperty.findById(dto.getFarmSectorPropertyId()).get();
		farmSectorProperty.setDateMeasured(existingProperty.getDateMeasured());
		if (farmSector.getFarmSectorProperties() == null) {
			farmSector.setFarmSectorProperties(new ArrayList<>());
		}

		farmSectorProperty.setFarmSector(farmSector);
		// repoFarmSector.save(farmSector);
		repoSectorProperty.save(farmSectorProperty);
		return farmSector;

	}

	// scan for sector detail property changes
	@GetMapping("/sectors/properties/find/{sectorDetailId}")
	public List<FarmSectorProperty> findSectorDetailPropertiesBySectorDetailId(
			@PathVariable("sectorDetailId") Integer sdi) {
		FarmSector farmSector = repoFarmSector.findById(sdi).get();
		List<FarmSectorProperty> propertiesForCurrentSector = repoSectorProperty.findByFarmSector(farmSector);

		if (propertiesForCurrentSector != null && propertiesForCurrentSector.size() > 0) {
			Collections.sort(propertiesForCurrentSector, new Comparator<FarmSectorProperty>() {

				@Override
				public int compare(FarmSectorProperty o1, FarmSectorProperty o2) {

					return (int) (o1.getDateMeasured().getTime() - o2.getDateMeasured().getTime());
				}

			});
		}
		return propertiesForCurrentSector;
	}

	@PostMapping("/sectors/properties/add")
	public FarmSector savePropertyToFarmSector(@RequestBody DTOFarmSectorProperty dto) {
		System.out.println("===================== SAVING DTO: " + dto);
		// saveSectorProperty(sectorId, sectorPropertyName, sectorPropertyValue)
		FarmSector farmSector = repoFarmSector.findById(dto.getFarmSectorId()).get();
		FarmSectorProperty farmSectorProperty = new FarmSectorProperty();
		farmSectorProperty.setPropName(dto.getPropertyName());
		farmSectorProperty.setPropValue(dto.getPropertyValue());
		if (farmSector.getFarmSectorProperties() == null) {
			farmSector.setFarmSectorProperties(new ArrayList<>());
		}
		
		farmSectorProperty.setDateMeasured(new Date());

		farmSectorProperty.setFarmSector(farmSector);
		farmSector.getFarmSectorProperties().add(farmSectorProperty);

		repoFarmSector.save(farmSector);

		FarmSector farmSectorUpdated = repoFarmSector.findById(dto.getFarmSectorId()).get();

		return farmSectorUpdated;
	}

	@PutMapping("/sectors/sectordetails/{sectorid}")
	public List<FarmSectorDetail> updateFarmSector(@PathVariable("sectorid") Integer sectorId,
			@RequestBody FarmSectorDetail jFarmSectorDetail) {
		FarmSector farmSector = repoFarmSector.findById(sectorId).get();
		System.out.println("UPDATING: " + jFarmSectorDetail);
		jFarmSectorDetail.setFarmSector(farmSector); // JsonIgnore pe field-ul
														// FarmSector din
														// FarmSectorDetail
		repoFarmSectorDetail.save(jFarmSectorDetail);
		if (jFarmSectorDetail.getFarmOperationProperty() != null) {
			repoOperationProperty.save(jFarmSectorDetail.getFarmOperationProperty());
		}

		List<FarmSectorDetail> farmSectorDatils = repoFarmSectorDetail.findByFarmSector(farmSector);
		return farmSectorDatils;
	}

	@DeleteMapping("/sectors/details/operations/delete/{delopid}/{idsectordetail}")
	public FarmSectorDetail deleteFarmOperationProperty(@PathVariable("delopid") Integer delOpId,
			@PathVariable("idsectordetail") Integer idSectorDetail) {
		FarmOperationProperty propDel = repoOperationProperty.findById(delOpId).get();
		repoOperationProperty.delete(propDel);

		FarmSectorDetail detail = repoFarmSectorDetail.findById(idSectorDetail).get();
		return detail;
	}

	@DeleteMapping("/sectors/details/operations/unbind/{delopid}/{idsectordetail}")
	public FarmSectorDetail unbindFarmOperationProperty(@PathVariable("delopid") Integer delOpId,
			@PathVariable("idsectordetail") Integer idSectorDetail) {

		FarmSectorDetail detail = repoFarmSectorDetail.findById(idSectorDetail).get();
		detail.setFarmOperationProperty(null);
		repoFarmSectorDetail.save(detail);
		return detail;

	}

	@PostMapping("/sectors/details/add/{sectorid}")
	public List<FarmSectorDetail> addFarmSectorDetail(@RequestBody FarmSectorDetail fsd,
			@PathVariable("sectorid") int sectorId) {
		FarmSector fs = repoFarmSector.findById(sectorId).get();
		fsd.setFarmSector(fs);

		System.out.println("SAVING FARM SECTOR DETAIL: " + fsd.getApplicationDate() + " ");
		System.out.println("Operation: " + fsd.getFarmOperationProperty());

		fsd.getFarmOperationProperty().setDateApplied(fsd.getApplicationDate());
		if (fsd.getFarmOperationProperty() != null && fsd.getFarmOperationProperty().getPropertyName() != null) {
			System.out.println("TODO REMOVE: " + fsd.getFarmOperationProperty().getPropertyName());
			System.out
					.println("OPERATION NAME: " + fsd.getFarmOperationProperty().getFarmOperation().getOperationName());
			if (fsd.getFarmOperationProperty().getFarmOperation().getOperationName()
					.equals(IConfigConstants.IRIGATION_PROPERTY_NAME)) {

				// if operation is applied today -> save openweathermap api
				// weather dto to json
				boolean isToday = DateUtils.isSameDay(fsd.getApplicationDate(), new Date());

				if (!isToday) {
					System.out.println(
							"[Debug] Not persisting weather information for current sector detail operation property");
				} else {

					FarmSector sector = fs;
					if (sector.getCoordinates() != null) {
						DTOPolygon dtoPolygon = PolygonService.string2Polygon(sector.getCoordinates());
						sector.setDtoPolygon(dtoPolygon);
						DTOWeather dto = WeatherService.loadFromEndpoint(
								sector.getDtoPolygon().getCoordinates().get(0).getLat(),
								sector.getDtoPolygon().getCoordinates().get(0).getLng());
						sector.setDtoWeather(dto);
					}

					try {
						ObjectMapper mapper = new ObjectMapper();
						String jsonWeather = fs.getDtoWeather() != null ? mapper.writeValueAsString(fs.getDtoWeather())
								: null;
						fsd.getFarmOperationProperty().setHistoricWeather(jsonWeather);
					} catch (JsonProcessingException e) {
						e.printStackTrace();
					}

				}
			}
		}

		Farm farm = fs.getFarm();
		Date dateAppliedForOperation = fsd.getFarmOperationProperty().getDateApplied();
		Calendar cal = Calendar.getInstance();
		cal.setTime(dateAppliedForOperation);
		cal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY) + 3); // Romanian time
		fsd.getFarmOperationProperty().setDateApplied(cal.getTime());
		repoFarmSectorDetail.save(fsd);

		List<FarmSectorDetail> currentDetails = repoFarmSectorDetail.findByFarmSector(fs);
		System.out.println("TEST");
		// List<FarmSector> farmSectorsForSelectedFarm =
		// repoFarmSector.findByFarm(farm);
		return currentDetails;
	}

	// TODO: create dto for farm operation property - to avoid long URLs
	@PostMapping("/sectors/details/operations/{farmsector}/{farmsectordetail}/{propertyname}/{propertyvalue}")
	public FarmSector addOperationToFarmSectorDetail(@RequestBody FarmOperation fo,
			@PathVariable("propertyname") String propertyName, @PathVariable("propertyvalue") String propertyValue,
			@PathVariable("farmsector") Integer idFarmSector,
			@PathVariable("farmsectordetail") Integer farmSectorDetailId) {

		System.out.println("SAVING OPERATION PROP TO SECTOR");

		FarmOperationProperty farmOperationProperty = new FarmOperationProperty();
		farmOperationProperty.setPropertyName(propertyName);
		farmOperationProperty.setPropertyValue(propertyValue);
		farmOperationProperty.setFarmOperation(fo);
		FarmSectorDetail farmSectorDetail = repoFarmSectorDetail.findById(farmSectorDetailId).get();
		farmSectorDetail.setFarmOperationProperty(farmOperationProperty);

		repoFarmSectorDetail.save(farmSectorDetail);
		// repoOperationProperty.save(farmOperationProperty);

		FarmSector farmSector = repoFarmSector.findById(idFarmSector).get();

		// List<FarmSectorDetail> details =
		// repoFarmSectorDetail.findByFarmSector(farmSector);

		return farmSector;
	}

	// sectors/remove/property
	@DeleteMapping("/sectors/remove/property/{idprop}")
	public FarmSector deleteFarmSectorProperty(@PathVariable("idprop") Integer idProperty) {

		FarmSectorProperty prop = repoSectorProperty.findById(idProperty).get();

		Farm farm = prop.getFarmSector().getFarm();
		repoSectorProperty.delete(prop);

		Integer farmSectorId = prop.getFarmSector().getId();
		FarmSector updatedFarmSector = repoFarmSector.findById(farmSectorId).get();
		// List<FarmSector> farmSectorsForSelectedFarm =
		// repoFarmSector.findByFarm(farm);
		return updatedFarmSector;
	}

	@DeleteMapping("/sectors/remove/detail/{idd}")
	public Iterable<FarmSector> deleteSectorDetail(@PathVariable("idd") Integer idDetail) {

		FarmSectorDetail detail = repoFarmSectorDetail.findById(idDetail).get();

		Farm farm = detail.getFarmSector().getFarm();
		repoFarmSectorDetail.delete(detail);
		List<FarmSector> farmSectorsForSelectedFarm = repoFarmSector.findByFarm(farm);
		return farmSectorsForSelectedFarm;
	}

	@DeleteMapping("/sectors/remove/{idsector}")
	public Iterable<FarmSector> deleteFarmSector(@PathVariable("idsector") Integer idsector) {

		FarmSector farmSector = repoFarmSector.findById(idsector).get();
		Farm farm = farmSector.getFarm();
		// Farm farm = repoFarm.findById(idfarm).get();
		// farmSector.setFarm(farm);
		repoFarmSector.delete(farmSector);
		List<FarmSector> farmSectorsForSelectedFarm = repoFarmSector.findByFarm(farm);
		return farmSectorsForSelectedFarm;
	}

	@RequestMapping("/sectors/byfarmid/{farmid}")
	public Iterable<FarmSector> allFarmSectorsByFarmId(@PathVariable("farmid") Integer farmId) {
		Farm farm = repoFarm.findById(farmId).get();
		List<FarmSector> farmSectorsForSelectedFarm = repoFarmSector.findByFarm(farm);
		return farmSectorsForSelectedFarm;
	}

}
