package ro.dragos.smartfarm.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ro.dragos.smartfarm.config.USER_TYPES;
import ro.dragos.smartfarm.dao.RepoUser;
import ro.dragos.smartfarm.dto.DTONewUser;
import ro.dragos.smartfarm.model.User;

@RestController
@RequestMapping("/register")
public class RestControllerRegister {

	@Autowired
	private RepoUser repoUser;
	
	
	@PostMapping("/newuser")
	public DTONewUser createNewUser(@RequestBody User user){
		
		DTONewUser dto = new DTONewUser();
		dto.setEmail(user.getEmail());
		dto.setUsername(user.getUsername());
		
		user.setUserType(USER_TYPES.FARM_OWNER.toString());
		
		if(repoUser.findByUsernameAndEmail(user.getUsername(), user.getEmail()).isPresent()){
			dto.setOperationSuccessful(false);
			dto.setOperationMessage("COULD NOT SAVE USER - email or username already in use");
			return dto;
		}
		
//		try{
//			repoUser.save(user);
//		}catch(Exception e){
//			dto.setOperationSuccessful(false);
//			dto.setOperationMessage("COULD NOT SAVE USER - email or username already in use");
//			return dto;
//		}
		
		repoUser.save(user);
		dto.setOperationMessage("REGISTERED SUCCESSFULLY");
		dto.setOperationSuccessful(true);
		return dto;
		
	}
	
}
