package ro.dragos.smartfarm.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ro.dragos.smartfarm.model.FarmOperation;

@Repository
public interface RepoOperations extends CrudRepository<FarmOperation, Integer>{

}
