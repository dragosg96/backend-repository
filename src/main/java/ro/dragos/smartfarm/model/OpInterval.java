package ro.dragos.smartfarm.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="farm_sector_interval")
public class OpInterval {

	@EmbeddedId
	private OpIntervalPK pk;
	
	@Column(name="interv")
	private Integer interval;
	
	public OpInterval() {
		// TODO Auto-generated constructor stub
	}

	public OpIntervalPK getPk() {
		return pk;
	}

	public void setPk(OpIntervalPK pk) {
		this.pk = pk;
	}

	public Integer getInterval() {
		return interval;
	}

	public void setInterval(Integer interval) {
		this.interval = interval;
	}

	@Override
	public String toString() {
		return "OpInterval [pk=" + pk + ", interval=" + interval + "]";
	}
	
	
	

}
