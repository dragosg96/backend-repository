package ro.dragos.smartfarm.dto;

public class DTOSaveFarm {

	
	private String city;
	private String street;
	private String number;
	
	private String farmName;
	
	public DTOSaveFarm() {
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getFarmName() {
		return farmName;
	}

	public void setFarmName(String farmName) {
		this.farmName = farmName;
	}
	
	
}
