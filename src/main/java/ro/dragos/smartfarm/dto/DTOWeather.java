package ro.dragos.smartfarm.dto;

public class DTOWeather {

	private Double temp; // main.temp
	private Double pressure; // main.pressure
	private Double humidity; // main.humidity
	private Double windSpeed; // wind.speed
	private String name; // name
	
	
	public DTOWeather() {
	}


	public DTOWeather(Double temp, Double pressure, Double humidity, Double windSpeed, String name) {
		this.temp = temp;
		this.pressure = pressure;
		this.humidity = humidity;
		this.windSpeed = windSpeed;
		this.name = name;
	}


	public Double getTemp() {
		return temp;
	}


	public void setTemp(Double temp) {
		this.temp = temp;
	}


	public Double getPressure() {
		return pressure;
	}


	public void setPressure(Double pressure) {
		this.pressure = pressure;
	}


	public Double getHumidity() {
		return humidity;
	}


	public void setHumidity(Double humidity) {
		this.humidity = humidity;
	}


	public Double getWindSpeed() {
		return windSpeed;
	}


	public void setWindSpeed(Double windSpeed) {
		this.windSpeed = windSpeed;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	@Override
	public String toString() {
		return "DTOWeather [temp=" + temp + ", pressure=" + pressure + ", humidity=" + humidity + ", windSpeed="
				+ windSpeed + ", name=" + name + "]";
	}
	
	
	
	
}
