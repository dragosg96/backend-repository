package ro.dragos.smartfarm.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import ro.dragos.smartfarm.dto.DTOPolygon;
import ro.dragos.smartfarm.dto.DTOWeather;


/**
 * The persistent class for the farm_sector database table.
 * 
 */
@Entity
@Table(name="farm_sector")
@NamedQuery(name="FarmSector.findAll", query="SELECT f FROM FarmSector f")
public class FarmSector implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="sector_description")
	private String sectorDescription;

	private double surface;
	
	@Column(name="coordinates")
	private String coordinates;
	
	@Transient
	private DTOPolygon dtoPolygon;
	
	@Transient
	private DTOWeather dtoWeather;

	//bi-directional many-to-one association to Farm
	@ManyToOne
	private Farm farm;

	//bi-directional many-to-one association to SoilType
	@ManyToOne
	@JoinColumn(name="soil_type")
	private SoilType soilTypeBean;

	//bi-directional many-to-one association to FarmSectorDetail
	@OneToMany(mappedBy="farmSector", cascade=CascadeType.ALL)
	private List<FarmSectorDetail> farmSectorDetails;

	//bi-directional many-to-one association to FarmSectorProperty
	@OneToMany(mappedBy="farmSector", cascade=CascadeType.ALL)
	private List<FarmSectorProperty> farmSectorProperties;

	public FarmSector() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	

	public DTOWeather getDtoWeather() {
		return dtoWeather;
	}

	public void setDtoWeather(DTOWeather dtoWeather) {
		this.dtoWeather = dtoWeather;
	}

	public String getSectorDescription() {
		return sectorDescription;
	}

	public void setSectorDescription(String sectorDescription) {
		this.sectorDescription = sectorDescription;
	}

	public double getSurface() {
		return this.surface;
	}

	public void setSurface(double surface) {
		this.surface = surface;
	}

	public Farm getFarm() {
		return this.farm;
	}

	public void setFarm(Farm farm) {
		this.farm = farm;
	}

	public SoilType getSoilTypeBean() {
		return this.soilTypeBean;
	}

	public void setSoilTypeBean(SoilType soilTypeBean) {
		this.soilTypeBean = soilTypeBean;
	}

	public List<FarmSectorDetail> getFarmSectorDetails() {
		return this.farmSectorDetails;
	}

	public void setFarmSectorDetails(List<FarmSectorDetail> farmSectorDetails) {
		this.farmSectorDetails = farmSectorDetails;
	}

	public FarmSectorDetail addFarmSectorDetail(FarmSectorDetail farmSectorDetail) {
		getFarmSectorDetails().add(farmSectorDetail);
		farmSectorDetail.setFarmSector(this);

		return farmSectorDetail;
	}

	public FarmSectorDetail removeFarmSectorDetail(FarmSectorDetail farmSectorDetail) {
		getFarmSectorDetails().remove(farmSectorDetail);
		farmSectorDetail.setFarmSector(null);

		return farmSectorDetail;
	}

	public List<FarmSectorProperty> getFarmSectorProperties() {
		return this.farmSectorProperties;
	}

	public void setFarmSectorProperties(List<FarmSectorProperty> farmSectorProperties) {
		this.farmSectorProperties = farmSectorProperties;
	}

	public FarmSectorProperty addFarmSectorProperty(FarmSectorProperty farmSectorProperty) {
		getFarmSectorProperties().add(farmSectorProperty);
		farmSectorProperty.setFarmSector(this);

		return farmSectorProperty;
	}

	public FarmSectorProperty removeFarmSectorProperty(FarmSectorProperty farmSectorProperty) {
		getFarmSectorProperties().remove(farmSectorProperty);
		farmSectorProperty.setFarmSector(null);

		return farmSectorProperty;
	}

	public String getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(String coordinates) {
		this.coordinates = coordinates;
	}

	public DTOPolygon getDtoPolygon() {
		return dtoPolygon;
	}

	public void setDtoPolygon(DTOPolygon dtoPolygon) {
		this.dtoPolygon = dtoPolygon;
	}
	
	
	
	

}