package ro.dragos.smartfarm.dto;

public class DTOCoordinate {

	
	private Double lat;
	private Double lng;
	
	public DTOCoordinate() {
	}

	public DTOCoordinate(Double lat, Double lng) {
		this.lat = lat;
		this.lng = lng;
	}

	
	
	
	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

	public Double getLng() {
		return lng;
	}

	public void setLng(Double lng) {
		this.lng = lng;
	}

	@Override
	public String toString() {
		return "DTOCoordinate [lat=" + lat + ", lng=" + lng + "]";
	}

	
	
	
}
