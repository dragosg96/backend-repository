package ro.dragos.smartfarm.service;

import java.util.Arrays;

import ro.dragos.smartfarm.dto.DTOCoordinate;
import ro.dragos.smartfarm.dto.DTOPolygon;

public class PolygonService {


	/**
	 * Method for converting string columns (data types) into DTOPolygon instance
	 * @param coord coordinates of a polygon representing CSV of pairs of lat and lng
	 * e.g. -34.49213141771243,149.6717099609375,-34.79040951761211,149.9793271484375,-34.47854784880349, 149.907916015625
	 * @return DTOPolygon instance
	 */
	public static DTOPolygon string2Polygon(String coord){

		DTOPolygon polygon = new DTOPolygon();
		String[] elements = coord.split(",");
		
		int numberOfPairs = (elements.length) / 2;
		for(int i=0; i<numberOfPairs; i++){
			Double lat = Double.parseDouble(elements[2*i]);
			Double lng = Double.parseDouble(elements[2*i+1]);
			DTOCoordinate coordDto = new DTOCoordinate(lat, lng);
			polygon.getCoordinates().add(coordDto);
			
		}
		
		return polygon;
	}
	
	public static String polygon2String(DTOPolygon dto){
		String result = ""; // TODO: update for StringBuilder / StringBuffer
		int current = 0;
		for(int i=0; i<dto.getCoordinates().size(); i++){
			result += dto.getCoordinates().get(i).getLat() + "," + dto.getCoordinates().get(i).getLng();
			if(current != dto.getCoordinates().size() - 1){
				result+=",";
			}
			current++;
		}
		return result;
	}
}
