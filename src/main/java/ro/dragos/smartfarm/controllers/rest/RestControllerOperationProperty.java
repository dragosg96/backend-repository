package ro.dragos.smartfarm.controllers.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ro.dragos.smartfarm.dao.RepoOperationProperty;
import ro.dragos.smartfarm.model.FarmOperationProperty;

@RestController
@RequestMapping("/rest/operationproperties")
public class RestControllerOperationProperty {

	@Autowired
	private RepoOperationProperty repoProperty;
	
	
	@GetMapping("/all")
	public Iterable<FarmOperationProperty> findAll(){
		return repoProperty.findAll();
	}
	
	
//	@PostMapping("/save/{farmsectorid}")
//	public FarmOperationProperty save(@RequestBody FarmOperationProperty prop,
//			@PathVariable("farmsectorid")Integer farmSectorId){
//		
//	}
		
}
